#!/usr/bin/env bash

# Wrapper script for the php program LoadResortData.php
# Run by cron every 2 hours during the day
#
# Sends output to a timestamped log file in the logs directory
# uses Healthchecks.io to monitor the script

# Takes one parameter which is the healthchecks URL

SCRIPT=../code/LoadWeatherAndConditions.php

# Check that we have 1 parameter and then set the URL
if [ $# -eq 1 ]
then
    URL=$1
else
    echo "Usage: $0 <healthchecks URL>"
    exit 1
fi

# Call Healthchecks.io to say we are starting
curl -fsS -m 10 --retry 5 -o /dev/null "$URL"/start

# Get the directory that this script is in
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Redirect all output to a timestamped log file
LOGFILE="${DIR}"/../logs/$(date +%Y%m%d_%H%M%S)_LoadResortData.log
exec > "$LOGFILE" 2>&1

# Run the script
php "$DIR"/$SCRIPT
result=$?

# Call Healthchecks.io to say we are finished and include the last 10000 characters of the log file
m=$(tail -c 10000 "$LOGFILE")
curl -fsS -m 10 --retry 5 --data-raw "$m" -o /dev/null "$URL"/$result

# End of script
exit 0

