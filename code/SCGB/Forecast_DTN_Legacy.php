<?php
declare(strict_types=1);

namespace SCGB;

use DateTime;
use Exception;

require_once(__DIR__ . '/Forecast_DTN.php');

/**
 * Class for Weather Forecasts from the Legacy Version of the API.
 *
 * Fetch a 6 hourly forecast and then an hourly forecast, each asking for different attributes
 * (see ARR_DTN_WEATHER_CODES) and then merge the two together.
 */
class Forecast_DTN_Legacy extends Forecast_DTN
{
    const WEATHER_AUTH_URL = 'https://auth.weather.mg/oauth/token';
    const WEATHER_FORECAST_URL =
        'https://point-forecast.weather.mg/forecast/hourly?validFrom=%s&validUntil' .
        '=%s&validPeriod=%s&locatedAt=%s&fields=%s';
    /**
     * Identify which attributes are available from which block.
     *
     * See full documentation at https://bitbucket.org/dtnse/weather-api/src/develop/
     *
     * We need to get two sets of data from CollectorDTN - the table below shows which forecast returns which data
     *
     * CollectorDTN Field Name                   PT0S PT6H Datatype  Description
     * averageWindSpeedInMilesPerHour         x    float    average wind speed in defined periods, 10m above ground
     * dominantWindDirectionInDegree          x    int      dominant wind direction in defined periods, 10m above ground
     * freezingLevelHeightInMeter        x         int      height of freezing level above sea level
     * freshSnowfallInCentimeter              x    float    amount of snow fallen in defined periods
     * maxAirTemperatureInCelsius             x    float    maximum of air temperature, 2m above ground
     * maxFeelsLikeTemperatureInCelsius       x    float    maximum of apparent temperature, 2m above ground
     * maxFreshSnowfallInCentimeter           x    float    maximum possible amount of snow to be expected in defined period
     * minAirTemperatureInCelsius             x    float    minimum of air temperature, 2m above ground in defined period
     * minFeelsLikeTemperatureInCelsius       x    float    maximum of apparent temperature, 2m above ground in
     *                                                       defined period
     * weatherCode                            x    int      weather code, current or integrated over defined period
     *
     * Note that forecasts are sometimes not available for all locations.  In that case no data is returned but
     * CollectorDTN support should be contacted to rectify this.
     *
     * See the wiki for more information
     */
    const  ARR_DTN_WEATHER_CODES = array(
        'PT6H' => array(
            'minAirTemperatureInCelsius',
            'maxAirTemperatureInCelsius',
            'minFeelsLikeTemperatureInCelsius',
            'maxFeelsLikeTemperatureInCelsius',
            'weatherCode',
            'freshSnowfallInCentimeter',
            'averageWindSpeedInMilesPerHour',
            'dominantWindDirectionInDegree',
        ),
        'PT0S' => array(
            'feelsLikeTemperatureInCelsius',
        ),
    );

    /**
     * This determines what text is displayed in the website. The limit is 16 characters.
     *
     * The comment on the right is the original text from the CollectorDTN.
     * TODO:  Determine whether to store this in the database or not.
     */
    const ARR_WEATHER_CODES = array(
        -1 => array('Heavy rain', '04-heavy-cloud-precip.png'),  // Heavy rain
        -2 => array('Heavy rain', '04-heavy-cloud-precip.png'),  // Heavy rain
        -3 => array('Heavy snow', '07-heavy-snow.png'),  // Heavy snow
        -4 => array('Dense fog', '09-heavy-cloud.png'),  // Dense Fog
        -5 => array('Freezing rain', '05-light-snow.png'),  // Freezing rain
        -6 => array('Storm', '04-heavy-cloud-precip.png'),  // Storm
        -7 => array('Snow storm', '07-heavy-snow.png'),  // Snow storm
        -8 => array('Thick high cloud', '09-heavy-cloud.png'),  // Thick cirrus clouds
        0 => array('Clear skies', '00-sunny-day.png'),  // Clear skies
        1 => array('Generally clear', '01-sunny-clouds.png'),  // Mainly clear skies
        10 => array('Mist', '08-light-cloud.png'),  // Mist
        11 => array('Ground fog', '09-heavy-cloud.png'),  // ground fog
        12 => array('Ground fog', '09-heavy-cloud.png'),  // ground fog
        13 => array('Sheet lightning', '11-thunder.png'),  // Sheet Lightning
        14 => array('Some showers', '02-light-cloud-precip.png'),  // Some showers
        15 => array('Some showers', '02-light-cloud-precip.png'),  // Some showers
        16 => array('Some showers', '02-light-cloud-precip.png'),  // Some showers
        17 => array('Thunderstorms?', '11-thunder.png'),  // Isolated thunderstorm
        18 => array('Wind gusts', '10-windy-cloud.png'),  // Wind gusts
        19 => array('Spouts', '03-med-cloud-precip.png'),  // Spouts
        2 => array('Mainly fair', '01-sunny-clouds.png'),  // Mainly Fair
        20 => array('Possible drizzle', '02-light-cloud-precip.png'),  // Possible Drizzle
        21 => array('Possible rain', '02-light-cloud-precip.png'),  // Possible Rain
        22 => array('Possible snow', '05-light-snow.png'),  // Possible Snow
        23 => array('Light rain/snow', '05-light-snow.png'),  // Light rain and snow
        24 => array('Freezing rain', '02-light-cloud-precip.png'),  // Light freezing rain
        25 => array('Light rain shwr', '02-light-cloud-precip.png'),  // Light rain shower
        26 => array('Light snow shwr', '05-light-snow.png'),  // Light snow shower
        27 => array('Light rain/snow', '05-light-snow.png'),  // Light shower of snow pellets or rain/snow mixed
        28 => array('Possible fog', '09-heavy-cloud.png'),  // Possible Fog
        29 => array('Thunderstorms', '11-thunder.png'),  // Isolated thunderstorm
        3 => array('Generally clear', '01-sunny-clouds.png'),  // Lengthy clear spells
        30 => array('Sandstorm', '04-heavy-cloud-precip.png'),  // Sandstorm
        31 => array('Sandstorm', '04-heavy-cloud-precip.png'),  // Sandstorm
        32 => array('Sandstorm', '04-heavy-cloud-precip.png'),  // Sandstorm
        33 => array('Heavy sandstorm', '04-heavy-cloud-precip.png'),  // Heavy sandstorm
        34 => array('Heavy sandstorm', '04-heavy-cloud-precip.png'),  // Heavy sandstorm
        35 => array('Heavy sandstorm', '04-heavy-cloud-precip.png'),  // Heavy sandstorm
        36 => array('Snow flurry', '05-light-snow.png'),  // Snow flurry
        37 => array('Snow flurries', '05-light-snow.png'),  // Heavy Snow flurry
        38 => array('Drifting snow', '05-light-snow.png'),  // Drifting snow
        39 => array('Drifting snow', '05-light-snow.png'),  // Heavy Drifting snow
        4 => array('Partly cloudy', '01-sunny-clouds.png'),  // Partly cloudy
        40 => array('Patchy fog', '08-light-cloud.png'),  // Patchy fog
        41 => array('Patchy fog', '08-light-cloud.png'),  // Patchy fog
        42 => array('Fog', '09-heavy-cloud.png'),  // Fog
        43 => array('Fog', '09-heavy-cloud.png'),  // Fog
        44 => array('Fog', '09-heavy-cloud.png'),  // Fog
        45 => array('Dense fog', '09-heavy-cloud.png'),  // Dense fog
        46 => array('Dense fog', '09-heavy-cloud.png'),  // Dense fog
        47 => array('Dense fog', '09-heavy-cloud.png'),  // Dense fog
        48 => array('Freezing fog', '09-heavy-cloud.png'),  // Freezing fog
        49 => array('Freezing fog', '09-heavy-cloud.png'),  // Freezing fog
        5 => array('Broken clouds', '08-light-cloud.png'),  // Broken clouds
        50 => array('Some drizzle', '02-light-cloud-precip.png'),  // Light Occasional drizzle
        51 => array('Light drizzle', '02-light-cloud-precip.png'),  // Light Drizzle
        52 => array('Some drizzle', '02-light-cloud-precip.png'),  // Occasional Drizzle
        53 => array('Drizzle', '02-light-cloud-precip.png'),  // Drizzle
        54 => array('Heavy drizzle', '04-heavy-cloud-precip.png'),  // Occasional Heavy Drizzle
        55 => array('Heavy drizzle', '04-heavy-cloud-precip.png'),  // Heavy Drizzle
        56 => array('Freezing drizzle', '02-light-cloud-precip.png'),  // Freezing drizzle
        57 => array('Freezing rain', '06-medium-snow.png'),  // Freezing rain
        58 => array('Drizzle & rain', '02-light-cloud-precip.png'),  // Drizzle and Rain
        59 => array('Drizzle & rain', '02-light-cloud-precip.png'),  // Drizzle and Rain
        6 => array('Rather cloudy', '08-light-cloud.png'),  // Rather cloudy
        60 => array('Occasional rain', '02-light-cloud-precip.png'),  // Occasional rain
        61 => array('Light rain', '02-light-cloud-precip.png'),  // Light rain
        62 => array('Occasional rain', '02-light-cloud-precip.png'),  // Occasional Rain
        63 => array('Rain', '03-med-cloud-precip.png'),  // Rain
        64 => array('Heavy rain', '04-heavy-cloud-precip.png'),  // Occasional Heavy rain
        65 => array('Heavy rain', '04-heavy-cloud-precip.png'),  // Heavy rain
        66 => array('Freezing rain', '03-med-cloud-precip.png'),  // Freezing rain
        67 => array('Freezing rain', '03-med-cloud-precip.png'),  // Freezing rain
        68 => array('Rain and snow', '05-light-snow.png'),  // Rain and snow
        69 => array('Heavy rain/snow', '04-heavy-cloud-precip.png'),  // Heavy rain and snow
        7 => array('Mainly cloudy', '09-heavy-cloud.png'),  // Mainly cloudy
        70 => array('Occasional snow', '05-light-snow.png'),  // Occasional snow
        71 => array('Light snow', '05-light-snow.png'),  // Light snow
        72 => array('Occasional Snow', '05-light-snow.png'),  // Occasional Snow
        73 => array('Snow', '06-medium-snow.png'),  // Snow
        74 => array('Heavy snow', '07-heavy-snow.png'),  // Occasional Heavy snow
        75 => array('Heavy snow', '07-heavy-snow.png'),  // Heavy snow
        76 => array('Diamond dust', '02-light-cloud-precip.png'),  // Diamond dust
        77 => array('Snow grains', '05-light-snow.png'),  // Snow grains
        78 => array('Snow crystals', '05-light-snow.png'),  // Snow crystals
        79 => array('Ice pellets', '08-light-cloud.png'),  // Ice pellets
        8 => array('Overcast', '09-heavy-cloud.png'),  // Overcast
        80 => array('Rain shower', '03-med-cloud-precip.png'),  // Rain shower
        81 => array('Heavy rain shwr', '04-heavy-cloud-precip.png'),  // Heavy rain shower
        82 => array('Heavy rain shwr', '04-heavy-cloud-precip.png'),  // Violent rain shower
        83 => array('Rain/Snow shower', '06-medium-snow.png'),  // Rain and snow shower
        84 => array('Rain/Snow shower', '06-medium-snow.png'),  // Heavy rain and snow shower
        85 => array('Snow shower', '05-light-snow.png'),  // Snow shower
        86 => array('Heavy snow shwr', '07-heavy-snow.png'),  // Heavy snow shower
        87 => array('Mixed rain/snow', '05-light-snow.png'),  // Shower of snow pellets or rain/snow mixed
        88 => array('Mixed rain/snow', '05-light-snow.png'),  // Heavy shower of snow pellets or rain/snow mixed
        89 => array('Hail shower', '06-medium-snow.png'),  // Hail shower
        9 => array('Unknown!', ''),  // ???
        90 => array('Heavy hail shwr', '04-heavy-cloud-precip.png'),  // Heavy hail shower
        91 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        92 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        93 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        94 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        95 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        96 => array('Thunderstorms', '11-thunder.png'),  // Thunderstorms
        97 => array('Thunderstorms', '11-thunder.png'),  // Heavy Thunderstorms
        98 => array('Thunderstorms', '11-thunder.png'),  // Heavy thunderstorms with sandstorm
        99 => array('Thunderstorms', '11-thunder.png'),  // Heavy thunderstorms with hail or gusts
    );

    /**
     * Properties as fetched from the API
     */
    protected DateTime $validFrom;
    protected DateTime $validUntil;
    protected int $dominantWindDirectionInDegree;
    protected float $minAirTemperatureInCelsius;
    protected float $maxAirTemperatureInCelsius;
    protected float $feelsLikeTemperatureInCelsius;
    protected int $weatherCode;
    protected float $freshSnowfallInCentimeter;
    protected ?float $averageWindSpeedInMilesPerHour = null;

    /**
     * Forecast_DTN_Legacy constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        return $this;
    }

    /**
     * @param array $arrForecast
     * @return void
     * @throws Exception
     */
    public function setForecastProperties(array $arrForecast) :void
    {
        $latitude = $arrForecast['locatedAt'][1];
        $longitude = $arrForecast['locatedAt'][0];
        $this->validFrom = new DateTime($arrForecast['validFrom']);
        $this->validUntil = new DateTime($arrForecast['validUntil']);
        $this->dominantWindDirectionInDegree = intval($arrForecast['dominantWindDirectionInDegree']);
        $this->averageWindSpeedInMilesPerHour = intval($arrForecast['averageWindSpeedInMilesPerHour']);
        $this->minAirTemperatureInCelsius = $arrForecast['minAirTemperatureInCelsius'];
        $this->maxAirTemperatureInCelsius = $arrForecast['maxAirTemperatureInCelsius'];
        $this->feelsLikeTemperatureInCelsius = $arrForecast['feelsLikeTemperatureInCelsius'];
        $this->weatherCode = $arrForecast['weatherCode'];
        $this->freshSnowfallInCentimeter = $arrForecast['freshSnowfallInCentimeter'];

        parent::setCoreProperties(
            $this->validFrom,
            $this->validUntil,
            $latitude,
            $longitude,
            intval($this->averageWindSpeedInMilesPerHour),
            $this->dominantWindDirectionInDegree,
            $this->freshSnowfallInCentimeter,
            $this->minAirTemperatureInCelsius,
            $this->maxAirTemperatureInCelsius,
            $this->feelsLikeTemperatureInCelsius,
        );
        $this->setWeather();
    }

    /**
     * Set the weather description based on the weather code. This limited to 16 characters
     *
     * @return void
     * @throws Exception
     */
    protected function setWeather(): void
    {
        // Check for a mapping for this weather code in $arrWeatherCodes
        if (array_key_exists($this->weatherCode, self::ARR_WEATHER_CODES)) {
            $this->strWeather = self::ARR_WEATHER_CODES[$this->weatherCode][0];
        } else {
            $this->strWeather = 'Unknown';
        }

        // Generate a non-fatal error if the weather description is too long.
        if (strlen($this->strWeather) > 16) {
            logger()->error(
                'Weather description too long - max (16) characters',
                array(
                    'weatherCode' => $this->weatherCode,
                    'weatherDescription' => $this->strWeather,
                    'length' => strlen($this->strWeather),
                    'file' => basename(__FILE__),
                    'function' => __FUNCTION__,
                    'line' => __LINE__,)
            );
        }
    }

    /**
     * Static method Process the legacy CollectorDTN data per location. This contains multiple forecasts.
     *
     * We get sent an array with two elements:
     * - One of one-hour forecasts - which contains feelsLikeTemperature
     * - One of 6-hour forecasts - which contains everything else
     *
     * These need to be merged into one
     *
     * $jsonDTNweather = {array[2]}
     *  0 = {array[1]}
     *      forecasts = {array[41]}
     *          0 = {array[12]}
     *              locatedAt = {array[2]}
     *              validFrom = "2023-07-27T20:00:00+02:00"
     *              validUntil = "2023-07-28T02:00:00+02:00"
     *              validPeriod = "PT6H"
     *              dominantWindDirectionInDegree = {int} 315
     *              minAirTemperatureInCelsius = {float} 9.9
     *              maxAirTemperatureInCelsius = {float} 15.9
     *
     * @param array $arrForecastsFromProvider
     * @return array
     * @throws Exception
     */
    public static function getForecastsForLocation(array $arrForecastsFromProvider): array
    {
        $arrForecasts = array();

        // Determine the position of the Hourly forecasts and then create an array keyed on date containing
        // feelsLikeTemperatureInCelcius
        $key = 0;
        if (!isset($arrForecastsFromProvider[0]['forecasts'][0]['validPeriod'])) {
            // The forecast is missing for this location
            return $arrForecasts;
        }
        if ($arrForecastsFromProvider[0]['forecasts'][0]['validPeriod'] == 'PT6H') {
            $key = 1;
        }

        $arrFeelsLikeTemp = array();
        for ($i = 0; $i < count($arrForecastsFromProvider[$key]['forecasts']); $i++) {
            $date = $arrForecastsFromProvider[$key]['forecasts'][$i]['validFrom'];
            $arrFeelsLikeTemp[$date] = $arrForecastsFromProvider[$key]['forecasts'][$i]['feelsLikeTemperatureInCelsius'];
        }

        if ($key == 0) {
            $key = 1;
        } else {
            $key = 0;
        }
        // Now iterate through the 6 hourly forecasts and add the feelsLikeTemperatureInCelcius where
        // validFrom is the same as the date
        foreach ($arrForecastsFromProvider[$key]['forecasts'] as $forecast) {
            $date = $forecast['validFrom'];
            // Check whether this date is in the array of feelsLikeTemperatures - if not continue
            if (!array_key_exists($date, $arrFeelsLikeTemp)) {
                continue;
            }
            $forecast['feelsLikeTemperatureInCelsius'] = $arrFeelsLikeTemp[$date];
            $x = new Forecast_DTN_Legacy();
            $x->setForecastProperties($forecast);
            $arrForecasts[] = $x;
        }

        return $arrForecasts;
    }

    /**
     * Return the Authorization URL for CollectorDTN API.
     *
     * @return string
     */
    static public function getAuthURL(): string
    {
        return self::WEATHER_AUTH_URL;
    }

    /**
     * Return an array of URLs for the CollectorDTN API - needs to be an array as we have multiple periods
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    static public function getForecastURLs(float $latitude, float $longitude): array
    {
        // Add dates in format 2023-06-25T00:00:00Z
        $validFrom = date('Y-m-d') . 'T00:00:00Z';
        // Add 10 days to the current date
        $validUntil = date('Y-m-d', strtotime('+10 days')) . 'T00:00:00Z';

        $arrURLs = array();
        foreach (self::ARR_DTN_WEATHER_CODES as $strPeriod => $strWeatherCodes) {
            $arrURLs[] = sprintf(
                self::WEATHER_FORECAST_URL,
                $validFrom,
                $validUntil,
                $strPeriod,
                $longitude . ',' . $latitude,
                self::getWeatherCodes($strPeriod)
            );
        }
        return $arrURLs;
    }

    /**
     * Return a comma separated list of weather codes for the CollectorDTN API - based on the period (which for the legacy
     * forecasts must be set)
     *
     * @param string|null $period
     * @return string
     * @throws Exception
     */
    static public function getWeatherCodes(string $period = null): string
    {
        if (!array_key_exists($period, self::ARR_DTN_WEATHER_CODES)) {
            throw new Exception('Invalid period passed to getDTNWeatherCodes -> ' . $period);
        }
        return implode(',', self::ARR_DTN_WEATHER_CODES[$period]);
    }

    /**
     * Set a link to the weather icon.
     *
     * We use a set of stock images distributed with the code and copy them
     * to a directory off the web root if they do not exist.
     *
     * @return void
     * @throws Exception
     */
    protected function setWeatherURL(): void
    {
        // Ensure that there is a weather icon
        // Directory where we keep raw material
        $dirStockImages = getConfigItem('dirStockImages');

        // Directory under the web root where we keep the images
        $dirWebRoot = getConfigItem('dirWebRoot');
        $dirWebImages = getConfigItem('dirWebImages');

        $imageFile = self::ARR_WEATHER_CODES[$this->weatherCode][1];
        $sourceFile = $dirStockImages . "/" . $imageFile;
        $this->strWeatherURL = "/" . $dirWebImages . "/" . $imageFile;
        $targetFile = $dirWebRoot . $this->strWeatherURL;

        // If the target file does not exist, copy it from the source
        if (!file_exists($targetFile)) {
            if (!($source = imagecreatefrompng($sourceFile))) {
                throw new Exception('Failed to open Weather image file: ' . $sourceFile);
            }
            imagesavealpha($source, true);
            $color = imagecolorallocatealpha($source, 0, 0, 0, 127);
            imagefill($source, 0, 0, $color);
            imagepng($source, $targetFile);
            imagedestroy($source);
        }
    }
}