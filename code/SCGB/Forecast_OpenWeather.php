<?php

namespace SCGB;
use Exception;
use DateTime;
use DateInterval;
use DateTimeZone;

class Forecast_OpenWeather extends Forecast_Base
{
    /**
     * Identify the periods AM, PM and Night as used on the website.
     *
     * These always referenced in local time (they pick up timezone from the CollectorDTN data)
     */
    const AM = 9;
    const PM = 15;
    const NIGHT = 21;
    const DAY = 0;

    /**
     * String representations of the periods AM, PM and Night as used on the website.
     */
    const ARR_PERIOD_STRINGS = array(
        self::AM => 'AM',
        self::PM => 'PM',
        self::DAY => 'DAY',
        self::NIGHT => 'NIGHT',
    );

    const THREE_HOUR_FORECAST = 0;
    const DAY_FORECAST = 1;
    const NIGHT_FORECAST = 2;

    const WEATHER_FORECAST_URL = 'https://api.openweathermap.org/data/2.5/forecast%s?units=metric&lat=%s&lon=%s%s';

    /**
     * Weather attributes unique to OpenWeather
     */
    private int $weather_id = 0;
    private string $weather_main = '';
    private string $weather_description = '';
    private string $weather_icon = '';

    private ?int $intForecastType = null;

    public function __construct()
    {
        parent::__construct();
        return $this;
    }

    /**
     * Unpack the data from OpenWeather. Contains a 3-hour forecast for 5 days and a daily forecast for 10 days.
     *
     * @param array $arrForecastsFromProvider
     * @return array
     * @throws Exception
     */
    static public function getForecastsForLocation(array $arrForecastsFromProvider): array
    {
        // The first element of the array is the 3-hour forecast for 5 days. Get that first and then add next 5 days
        // from the daily forecast
        $arrForecasts = null;
        $arr3HourlyForecasts = $arrForecastsFromProvider[0]['list'];
        $arrDailyForecasts = $arrForecastsFromProvider[1]['list'];
        $dtmForecast = null;

        // Get the timezone from the city element - this is seconds from UTC. Create a DateTimeZone object
        $timezone = $arrForecastsFromProvider[0]['city']['timezone'];
        $timezone = new DateTimeZone(sprintf('%+03d:%02d', $timezone / 3600, abs($timezone) % 3600 / 60));

        // Get the co-ords from the city element
        $latitude = $arrForecastsFromProvider[0]['city']['coord']['lat'];
        $longitude = $arrForecastsFromProvider[0]['city']['coord']['lon'];

        foreach ($arr3HourlyForecasts as $forecast)
        {
            // Get the date and time of the forecast from the element 'dt' which is in Unix time
            // set the timezone based on $timezone
            $dtmForecast = new DateTime();
            $dtmForecast->setTimestamp($forecast['dt']);
            $dtmForecast->setTimezone($timezone);

            $x = new Forecast_OpenWeather();
            $x->setForecastProperties($forecast, $dtmForecast, $latitude, $longitude, self::THREE_HOUR_FORECAST);
            $arrForecasts[] = $x;
        }
        $dtmLatest3HourlyForecast = $dtmForecast;

        // Add the daily forecasts - but skip any days already in the 3-hourly forecast
        // TODO handle the 3-hourly forecasts overlapping into the daily forecasts
        foreach ($arrDailyForecasts as $forecast)
        {
            // Get the date and time of the forecast from the element 'dt' which is in Unix time
            // set the timezone based on $timezone
            $dtmForecast = new DateTime();
            $dtmForecast->setTimestamp($forecast['dt']);
            $dtmForecast->setTimezone($timezone);

            // If the date of this forecast is less than the date of $dtmLatest3HourlyForecast then continue
            if ($dtmForecast < $dtmLatest3HourlyForecast)
            {
                continue;
            }

            // The daily forecasts have a day and night forecast. Add both
            $x = new Forecast_OpenWeather();
            $x->setForecastProperties($forecast, $dtmForecast, $latitude, $longitude, self::DAY_FORECAST);
            $arrForecasts[] = $x;
            $x = new Forecast_OpenWeather();
            $x->setForecastProperties($forecast, $dtmForecast, $latitude, $longitude, self::NIGHT_FORECAST);
            $arrForecasts[] = $x;
        }

        return $arrForecasts;
    }

    /**
     * @throws Exception
     */
    public function setForecastProperties(
        array $arrForecast, DateTime $dtmForecast, float $latitude, float $longitude, int $forecastType): void
    {
        // Set the forecast period
        $this->intForecastType = $forecastType;
        // Some values depend on the forecast type and not always set
        $fltSnowExpectedinCentimeters = 0;

        // Set the Weather attributes - Common across all forecasts
        $this->weather_id = $arrForecast['weather'][0]['id'];
        $this->weather_main = $arrForecast['weather'][0]['main'];
        $this->weather_description = $arrForecast['weather'][0]['description'];
        $this->weather_icon = $arrForecast['weather'][0]['icon'];

        if ($forecastType == self::THREE_HOUR_FORECAST) {
            $dtmForecastEndDate = clone $dtmForecast;
            $interval = new DateInterval('PT3H');
            $dtmForecastEndDate->add($interval);

            // Get the snowfall - may not always be set - key is snow.3h
            if (isset($arrForecast['snow']['3h'])) {
                $fltSnowExpectedinCentimeters = $arrForecast['snow']['3h'];
            }

            // Get the wind speed and direction
            $intWindDirectionInDegrees = $arrForecast['wind']['deg'];
            $intWindSpeedInMPH = round($arrForecast['wind']['speed'] * 2.23694);

            // Get the min and max temperatures
            $fltMinAirTemperature = $arrForecast['main']['temp_min'];
            $fltMaxAirTemperature = $arrForecast['main']['temp_max'];

            // and the feels like temperature
            $fltFeelsLikeTemperature = $arrForecast['main']['feels_like'];
        } else
        {
            // Get the snowfall - may not always be set - key is snow
            if (isset($arrForecast['snow']))
            {
                // TODO Snow forecast is for the whole day - need to work out how to split this between day and night
                $fltSnowExpectedinCentimeters = $arrForecast['snow'] / 2;
            }

            // Get the wind speed and direction - set for the entire day
            $intWindDirectionInDegrees = $arrForecast['deg'];
            $intWindSpeedInMPH = round($arrForecast['speed'] * 2.23694);

            $interval = new DateInterval('PT12H');
            if ($forecastType == self::DAY_FORECAST)
            {
                // Start and end time is the first 12 hours of the period
                $dtmForecastEndDate = clone $dtmForecast;
                $dtmForecastEndDate->add($interval);
                $fltMinAirTemperature = $arrForecast['temp']['min'];
                $fltMaxAirTemperature = $arrForecast['temp']['max'];
                $fltFeelsLikeTemperature = $arrForecast['feels_like']['day'];
            }else {
                // Start and end time is the second 12 hours of the period
                $dtmForecast->add($interval);
                $dtmForecastEndDate = clone $dtmForecast;
                $dtmForecastEndDate->add($interval);
                $fltMinAirTemperature = $arrForecast['temp']['eve'];
                $fltMaxAirTemperature = $arrForecast['temp']['morn'];
                $fltFeelsLikeTemperature = $arrForecast['feels_like']['night'];
            }
        }

        parent::setCoreProperties(
            clone($dtmForecast),
            $dtmForecastEndDate,
            $latitude,
            $longitude,
            $intWindSpeedInMPH,
            $intWindDirectionInDegrees,
            $fltSnowExpectedinCentimeters,
            $fltMinAirTemperature,
            $fltMaxAirTemperature,
            $fltFeelsLikeTemperature);

        $this->setWeather();
    }

    /**
     * @inheritDoc
     */
    static public function getForecastURLs(float $latitude, float $longitude): array
    {
        return array(
            sprintf(
                self::WEATHER_FORECAST_URL,
                '',
                $latitude,
                $longitude,
                '',
            ),
            sprintf(
                self::WEATHER_FORECAST_URL,
                '/daily',
                $latitude,
                $longitude,
                '&cnt=10',
            )
        );
    }

    /**
     * @inheritDoc
     */
    protected function setWeather(): void
    {
        $this->strWeather = $this->weather_description;
    }

    /**
     * @inheritDoc
     */
    protected function setWeatherURL(): void
    {
        // TODO: Implement setWeatherURL() method.
    }

    /**
     * For OpenWeather we have 3 Hourly and 24 Hourly periods so have treat this in a specific way
     * Determine what string represents the forecast period: AM, PM, DAY or NIGHT
     *
     * @param DateTime $dtmForecastStart
     * @param DateTime $dtmForecastEnd
     * @return void
     * @throws Exception
     */
    public function setForecastPeriod(
        DateTime $dtmForecastStart,
        DateTime $dtmForecastEnd): void
    {
        if ($this->intForecastType == self::THREE_HOUR_FORECAST) {
            parent::setForecastPeriod($dtmForecastStart, $dtmForecastEnd);
        } elseif ($this->intForecastType == self::DAY_FORECAST  || $this->intForecastType == self::NIGHT_FORECAST) {
            // Morning or the Afternoon?
            $validFromHour = intval($dtmForecastStart->format('G'));
            if ($validFromHour < 12) {
                $intForecastPeriod = self::DAY;
            } else {
                $intForecastPeriod = self::NIGHT;
            }
            $this->strForecastPeriod = self::ARR_PERIOD_STRINGS[$intForecastPeriod];
        } else {
            logger()->error('Invalid forecast type',
                array(
                    'forecastType' => $this->intForecastType,
                    'file' => __FILE__,
                    'function' => __FUNCTION__,
                    'line' => __LINE__,
                )
            );
            throw new Exception('Invalid time difference between forecast start and end');
        }
    }
}