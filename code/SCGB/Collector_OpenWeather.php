<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

require_once (__DIR__ . '/Utils.php');
require_once (__DIR__ . '/Collector_Base.php');
require_once (__DIR__ . '/Forecast_OpenWeather.php');

/**
 * Class CollectorOpenWeather
 * @package SCGB
 */

class Collector_OpenWeather extends Collector_Base
{
    private string $apiKey;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        try {
            $this->apiKey = getConfigItem('openWeatherAPIKey');
        } catch (Exception $e) {
            logger()->emergency(
                'Failed to get OpenWeather API Key: ' . $e->getMessage(),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__)
            );
            throw $e;
        }

        parent::__construct('OW');
        return $this;
    }

    /**
     * @throws Exception
     */
    public function getForecastForLocation(float $latitude, float $longitude): array
    {
        logger()->debug('Getting Forecast from OpenWeather for ' . $latitude . '/' . $longitude,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        $forecast = new Forecast_OpenWeather();
        return $this->getForecastForLocationFromProvider($latitude, $longitude, $forecast);
    }

    /**
     * @throws Exception
     */
    protected function getForecastURLs(float $latitude, float $longitude): array
    {
        // Get the URLa and add the app id onto the end
        $forecastURLs = array();
        foreach (Forecast_OpenWeather::getForecastURLs($latitude, $longitude) as $url) {
            $forecastURLs[] = $url . '&appid=' . $this->apiKey;
        }
        return $forecastURLs;
    }
}