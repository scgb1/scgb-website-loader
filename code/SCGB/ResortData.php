<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use mysqli;

require_once (__DIR__ . '/ElevationData.php');
require_once (__DIR__ . '/SkiService.php');
require_once (__DIR__ . '/Collector_Base.php');
require_once (__DIR__ . '/Forecast_Base.php');

/**
 * Class to hold Resort Data. This is initially populated from a CSV and then enriched with elevation data.
 */
class ResortData
{
    /**
     * Array of Country Names that we check SCGB supplied data against. Only generate a warning if the country
     * is not in this list.
     */
    const ARR_VALID_COUNTRY_CODES = array(
        'Andorra',
        'Argentina',
        'Australia',
        'Austria',
        'Bulgaria',
        'Canada',
        'Chile',
        'Czech Republic',
        'Finland',
        'France',
        'Germany',
        'Italy',
        'Japan',
        'New Zealand',
        'Norway',
        'Poland',
        'Romania',
        'Scotland',
        'Slovakia',
        'Slovenia',
        'Spain',
        'Sweden',
        'Switzerland',
        'USA',
    );

    /**
     * Properties containing data loaded from the SkiClub file - The column headers must match these names.
     */
    protected int $intSCGBResortID = 0;             // Mandatory
    protected string $strResortName = '';           // Mandatory
    protected string $strResortCountry = '';        // Mandatory
    protected ?int $intResortSkiServiceID = null;      // Optional - Data not always available - NULL if not set
    protected float $fltResortLongitude = 0.0;      // Mandatory
    protected float $fltResortLatitude = 0.0;       // Mandatory
    protected float $fltMountainLongitude = 0.0;    // Mandatory
    protected float $fltMountainLatitude = 0.0;     // Mandatory

    /**
     * Added after initial load from Resort File.
     *
     * If available, se Elevations cached locally in a file else attempt to get them from Google
     */
    protected ?float $intResortAltitude = null;
    protected ?float $intMountainAltitude = null;

    /**
     * Data from Ski Service - all optional and null if not set/available
     */
    protected ?string $strSkiServiceResortName = null;
    protected ?int $intSnowLowerPisteInCentimeters = null;
    protected ?int $intSnowUpperPisteInCentimeters = null;
    protected ?DateTime $dtmLastSnowed = null;
    protected ?int $intLiftsOpen = null;
    protected ?int $intLiftsTotal = null;
    protected ?string $strOffPisteSnowConditions = null;
    protected ?DateTime $dtmResortOpening = null;
    protected ?DateTime $dtmResortClosing = null;

    /**
     * Current Weather - extracted from the Weather forecast and added while processing the forecasts
     */
    protected ?float $fltMinTemperature = null;
    protected ?float $fltMaxTemperature = null;
    protected string $strWeather = '';
    protected string $strWeatherURL = '';
    protected float $fltSnowForecastNext3DaysInCentimeters = 0.0;
    protected float $fltSnowForecastNext6DaysInCentimeters = 0.0;

    /**
     * Used to check for duplicates - not exposed outside the class
     */
    private static array $arrResortDataByID = array(); // array of intSCGBResortID
    private static array $arrResortDataByName = array(); // array of strResortName

    /**
     * Array of valid forecasts for the next 10 days
     */
    private array $arrForecasts = array();

    /**
     * Constructor - takes an associated array containing a line from the Resort Data CSV.
     *
     * The config item 'resortDataFile' points at the CSV
     *
     * @param $row - associative array with row level data from the CSV
     * @returns - the new object
     * @throws Exception
     */
    public function __construct(array $row)
    {
        // There could be a lot of other data in the array but these the ones required.
        // only intSCGBResortID, strResortName and strResortCountry mandatory
        // The others set to NULL if not present or cant be converted
        // TODO Handle Missing Data Better
        $this->intSCGBResortID = ResortData::getrowvalue(
            $row, 'intSCGBResortID', 'int', true
        );
        $this->strResortCountry = ResortData::getrowvalue(
            $row, 'strResortCountry', 'string', true
        );
        $this->strResortName = ResortData::getrowvalue(
            $row, 'strResortName', 'string', true
        );
        $this->intResortSkiServiceID = ResortData::getrowvalue(
            $row, 'intResortSkiServiceID', 'int', false
        );
        $this->fltResortLatitude = ResortData::getrowvalue(
            $row, 'fltResortLatitude', 'float', true
        );
        $this->fltResortLongitude = ResortData::getrowvalue(
            $row, 'fltResortLongitude', 'float', true
        );
        $this->fltMountainLatitude = ResortData::getrowvalue(
            $row, 'fltMountainLatitude', 'float', true
        );
        $this->fltMountainLongitude = ResortData::getrowvalue(
            $row, 'fltMountainLongitude', 'float', true
        );

        // Check whether this is not a dupe and check whether the lat/longs valid
        $this->checkDataValidity();

        return $this;
    }

    /** @noinspection PhpUnused */
    public function getResortSkiServiceID(): int|null
    {
        return $this->intResortSkiServiceID;
    }

    /**
     * Add weather forecast to the resort.
     *
     * We get an array of WeatherForecastBase objects and need to select the ones we want.
     *
     * @param array $arrForecasts
     * @param bool $isTopForecast
     * @return void
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function addForecast(array $arrForecasts, bool $isTopForecast): void
    {
        logger()->debug(
            "Processing forecast for resort",
            array(
                'intSCGBResortID' => $this->intSCGBResortID,
                'strResortName' => $this->strResortName,
                'isTopForecast' => $isTopForecast,
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        // iterate through the forecast data and pick out the times we want
        foreach ($arrForecasts as $forecast) {
            $forecast->setIsTopForecast($isTopForecast);

            // Get the dates of the forecast - and then get current time in the same timezone
            $dtmForecast = $forecast->getForecastDate();
            $dtmForecastEnd = $forecast->getForecastEndDate();

            // Get the current time in the same timezone as the forecast
            $dtmNow = new DateTime();
            $timezone = $dtmForecast->getTimezone();
            $dtmNow->setTimezone($timezone);

            // Update the 3 & 6 day Snow Forecast on the resort - need to be timezone aware
            // Only do this for forecasts from the highest point on the resort
            // Keep track of the snow for the period not displayed
            $fltSnowExpected = $forecast->getSnowExpectedInCentimeters();
            if ($isTopForecast &&  $fltSnowExpected > 0.0 && $dtmForecastEnd > $dtmNow) {
                $this->addSnow($dtmNow, $dtmForecast, $fltSnowExpected);
            }

            // See if it's a period of interest - if it is get a string indicating the period
            $forecast->setForecastPeriod($dtmForecast, $dtmForecastEnd);

            // Now see if going to add this one on to $arrWeatherForecasts
            if ($forecast->getForecastPeriod() !== null) {
                $this->arrForecasts[] = $forecast;
            }

            // Update the resort with the current weather. The preference is to use the resort forecast
            // rather than the top forecast. But occasionally the resort forecast is not available
            if ($dtmNow >= $dtmForecast && $dtmNow < $dtmForecastEnd) {
                if (!$isTopForecast || $this->strWeather == '') {
                    // Update the current weather on the resort
                    $this->strWeather = $forecast->getWeather();
                    $this->fltMinTemperature = $forecast->getMinTemperature();
                    $this->fltMaxTemperature = $forecast->getMaxTemperature();
                    $this->strWeatherURL = $forecast->getWeatherURL();
                }
            }
        }
    }

    /**
     * static method - Load the base resort information from a CSV and then add Elevation Data.
     *
     * @return array - array of ResortData objects
     * @throws Exception
     */
    public static function getResortData(): array
    {
        $strDataFile = getDataFileName('resortDataFile');

        // Map lines of the string returned by file function to $rows array.
        $rows = array_map('str_getcsv', file($strDataFile));
        logger()->debug(
            "Loading resort data",
            array(
                'data file' => $strDataFile,
                'rows' => count($rows),
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        //Get the first row that is the HEADER row.
        $header_row_tmp = array_shift($rows);

        // Need to remove non printables - Weird side effect from saving excel as a csv
        $i = 0;
        $header_row = array();
        foreach ($header_row_tmp as $ignored) {
            $header_row[$i] = preg_replace('/[[:^print:]]/', '', $header_row_tmp[$i++]);
        }

        //This array holds the final response - which is keyed on the first column of the header
        $arrResortData = [];

        foreach ($rows as $row) {
            $tmpResortData = null;
            $tmpResortData['intSCGBResortID'] = $row[0];
            for ($i = 1; $i < count($row); $i++) {
                $tmpResortData[$header_row[$i + 1]] = $row[$i];
            }
            $classResortData = new ResortData($tmpResortData);
            $arrResortData[$classResortData->intSCGBResortID] = $classResortData;
        }

        ResortData::addElevationData($arrResortData);
        return $arrResortData;
    }

    /**
     * Reconcile the resort data in the SCGB file with the data in WordPress.
     * - Resorts in WordPress not in the SCGB file or that don't have a SCGB ID
     * - Names that don't match - doing this to make sure the ID's set correctly
     *
     * @param mysqli $mysqli - database connection
     * @return void
     * @throws Exception
     */
    static public function reconcileResortDataWithWebsite(mysqli $mysqli): void
    {
        $arrWebsiteDataByID = array();
        $arrWebsiteDataByName = array();

        // Get the list of resorts in WordPress
        $arrResortsInWordPress = ResortData::getResortsInWordPress($mysqli);

        // Iterate through the resorts in WordPress and check they in the SCGB data
        foreach ($arrResortsInWordPress as $resortInWordPress) {
            $intWebsiteAPIID = $resortInWordPress['intWebsiteAPIID'];
            $strWebsiteResortName = $resortInWordPress['strWebsiteResortName'];

            $arrWebsiteDataByName[$strWebsiteResortName] = $intWebsiteAPIID;

            // Check for situation where there is no SCGB Data
            if ($intWebsiteAPIID === null) {
                logger()->error(
                    "No API Set for for Resort",
                    array(
                        'strWebsiteResortName' => $strWebsiteResortName,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
                continue;
            }

            $arrWebsiteDataByID[$intWebsiteAPIID] = $strWebsiteResortName;

            $intSCGBResortID = null;
            $strResortName = null;

            if (array_key_exists($intWebsiteAPIID, ResortData::$arrResortDataByID)) {
                $strResortName = ResortData::$arrResortDataByID[$intWebsiteAPIID]->strResortName;
            }

            if (array_key_exists($strWebsiteResortName, ResortData::$arrResortDataByName)) {
                $intSCGBResortID = ResortData::$arrResortDataByName[$strWebsiteResortName]->intSCGBResortID;
            }

            // Now do the checking in a way that will make it easier to fix any issues = check names first
            if ($strResortName === null) {
                // No name in SCGB data
                logger()->error(
                    "Website Resort not in SCGB Data",
                    array(
                        'strWebsiteResortName' => $strWebsiteResortName,
                        'intWebsiteAPIID' => $intWebsiteAPIID,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
                continue;
            }
            if ($strWebsiteResortName == $strResortName) {
                // Names match - check ID's
                if ($intWebsiteAPIID == $intSCGBResortID) {
                    // All good
                    logger()->debug('Website is correct', array(
                        'intWebsiteAPIID' => $intWebsiteAPIID,
                        'strWebsiteResortName' => $strWebsiteResortName,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    ));
                } else {
                    // ID's don't match
                    logger()->error(
                        "ID Mismatch Between Website and SCGB Data",
                        array(
                            'strWebsiteResortName' => $strWebsiteResortName,
                            'intWebsiteAPIID' => $intWebsiteAPIID,
                            'intSCGBResortID' => $intSCGBResortID,
                            'file' => basename(__FILE__),
                            'function' => __FUNCTION__,
                            'line' => __LINE__,
                        )
                    );
                }
            } else {
                // IDs the same but Names don't match
                logger()->error(
                    "Name Mismatch Between Website and SCGB Data",
                    array(
                        'intWebsiteAPIID' => $intWebsiteAPIID,
                        'strWebsiteResortName' => $strWebsiteResortName,
                        'strResortName' => $strResortName,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
            }
        }

        // Now go through the SCGB data and check all the resorts in the website data
        foreach (ResortData::$arrResortDataByID as $resortData) {
            $intSCGBResortID = $resortData->intSCGBResortID;
            $strResortName = $resortData->strResortName;
            if (!array_key_exists($intSCGBResortID, $arrWebsiteDataByID) ||
                !array_key_exists($strResortName, $arrWebsiteDataByName))
            {
                logger()->warning(
                    "SCGB Resort not in Website Data",
                    array(
                        'intSCGBResortID' => $intSCGBResortID,
                        'strResortName' => $strResortName,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
            }
        }
    }

    /**
     * @throws Exception
     */
    private static function getResortsInWordPress(mysqli $mysqli): array
    {
        $arrResortsInWordPress = array();

        $strSQL = "SELECT post_title, meta_value " .
            "FROM wp_postmeta m, wp_posts p " .
            "where post_id = id and " .
            "meta_key = 'resort_weather_resort_api_id' and " .
            "post_status = 'publish'";
        $result = $mysqli->query($strSQL);
        if ($result === false) {
            throw new Exception("Error getting resorts from WordPress: " . $mysqli->error);
        }

        while ($row = $result->fetch_assoc()) {
            $intWebsiteAPIID = $row['meta_value'];
            if ($intWebsiteAPIID == '') {
                $intWebsiteAPIID = null;
            }
            $arrResortsInWordPress[] = array(
                'strWebsiteResortName' => $row['post_title'],
                'intWebsiteAPIID' => $intWebsiteAPIID,
            );
        }

        return $arrResortsInWordPress;
    }

    /**
     * Add data from Ski Service (if available) to the resort data supplied by SCGB.
     *
     * @param array $arrResortData - foundation array of resort data. This is modified by this function
     * @param SkiService $skiServiceCollector - the Ski Service data collector
     * @return void
     * @throws Exception
     */
    static public function addSkiServiceData(array $arrResortData, SkiService $skiServiceCollector): void
    {
        /* Get the Ski Service Data as a usable array */
        $arrSkiServiceData = $skiServiceCollector->getSkiServiceResortData();

        // Iterate through the SCGB data and add the Ski Service data to it
        foreach ($arrResortData as $SCGBResortData) {
            // We don't have a Ski Service ID for all SCGB resorts, so we need to check that we have one before we try to
            $intSkiServiceID = $SCGBResortData->getResortSkiServiceID();
            if ($intSkiServiceID) {
                logger()->debug(
                    "Adding Ski Service data to SCGB resort",
                    array(
                        'Resort Name' => $SCGBResortData->strResortName,
                        'SkiServiceID' => $intSkiServiceID,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );

                // Check the Ski Service ID exists in the Ski Service data
                if (!array_key_exists($intSkiServiceID, $arrSkiServiceData)) {
                    logger()->error(
                        "Ski Service ID not found",
                        array(
                            'SkiServiceID' => $intSkiServiceID,
                            'file' => basename(__FILE__),
                            'function' => __FUNCTION__,
                            'line' => __LINE__,
                        )
                    );
                    continue;
                }
                $arrSkiServiceDataForResort = $arrSkiServiceData[$SCGBResortData->getResortSkiServiceID()];
                $SCGBResortData->strSkiServiceResortName = $arrSkiServiceDataForResort['strResortName'];
                $SCGBResortData->intSnowUpperPisteInCentimeters =
                    checkType($arrSkiServiceDataForResort['intSnowUpperPiste'], 'int');
                $SCGBResortData->intSnowLowerPisteInCentimeters =
                    checkType($arrSkiServiceDataForResort['intSnowLowerPiste'], 'int');
                $SCGBResortData->dtmLastSnowed =
                    getDateTime(checkType($arrSkiServiceDataForResort['dtmLastSnowed'], 'date'));
                $SCGBResortData->intLiftsTotal = checkType($arrSkiServiceDataForResort['intTotalLifts'], 'int');
                $SCGBResortData->intLiftsOpen = checkType($arrSkiServiceDataForResort['intLiftsOpen'], 'int');
                $SCGBResortData->strOffPisteSnowConditions =
                    checkType($arrSkiServiceDataForResort['strOffPisteSnowConditions'], 'string');
                $SCGBResortData->dtmResortClosing =
                    getDateTime(checkType($arrSkiServiceDataForResort['dtmResortClosing'], 'date'));
                $SCGBResortData->dtmResortOpening =
                    getDateTime(checkType($arrSkiServiceDataForResort['dtmResortOpening'], 'date'));
            } else {
                logger()->debug(
                    "No Ski Service ID for resort",
                    array(
                        'strResortName' => $SCGBResortData->strResortName,
                        'file' => basename(__FILE__),
                        'function' => __FUNCTION__,
                        'line' => __LINE__,
                    )
                );
            }
        }

        // Print the resort for which there is data but which is not used - only done in debug
        $skiServiceCollector->printUnusedResorts($arrResortData);

    }

    /**
     * Fetch and Process the Weather Forecast from CollectorDTN for the contents of $arrResortData.
     *
     * Iterate through the list of Ski Resorts, fetch the associated weather forecastS from CollectorDTN and add them to the
     * resort object. Each forecast has to be fetched one at a time - so this could take some time.
     *
     * The legacy API supports fetching multiple locations at once but this is deprecated in the new one.
     *
     * For development purposes it is possible to save the JSON data to a file and then load it from there instead of
     * fetching it from CollectorDTN. This is controlled by the config items 'saveForecastDataToDir' and 'loadForecastDataFromDir'
     *
     * @param array $arrResortData
     * @param Collector_Base $collector
     * @return void
     * @throws Exception
     */
    static public function addWeatherForecasts(array $arrResortData, Collector_Base $collector): void
    {
        logger()->debug("Adding Weather Forecasts to the resorts",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // Iterate through the resorts and produce 2 forecasts for each one - resort & mountain
        foreach ($arrResortData as $resort) {
            foreach (array(TRUE, FALSE) as $isMountainStation) {
                if ($isMountainStation) {
                    $latitude = $resort->fltMountainLatitude;
                    $longitude = $resort->fltMountainLongitude;
                } else {
                    $latitude = $resort->fltResortLatitude;
                    $longitude = $resort->fltResortLongitude;
                }
                $forecastsForLocation = $collector->getForecastForLocation($latitude, $longitude);
                // Check if forecast returned - if not continue to the next resort
                if (count($forecastsForLocation) == 0) {
                    continue;
                }
                // Add the forecasts to the resort
                // As well as getting data ready for loading into the forecast table we will also update the Current resort
                // conditions
                $resort->addForecast($forecastsForLocation, $isMountainStation);
            }
        }
    }

    /**
     * Save all the resort & forecast data to a CSV file.
     *
     * TODO - Add CollectorDTN API version to the file name
     *
     * @param array $arrResortData - array of ResortData objects
     * @param string $strResortDataDir - directory to save the data in. This will be timestamped
     * @return void
     */
    public static function saveToCSV(array $arrResortData, string $strResortDataDir): void
    {
        logger()->debug(
            "Saving resort data to CSV",
            array(
                'directory' => $strResortDataDir,
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        // Convert this to a timestamped dir name and then create it if it doesn't exist
        $strResortDataDir = getTimeStampedFilename($strResortDataDir);
        if (!file_exists($strResortDataDir)) {
            mkdir($strResortDataDir, 0755, true);
        }

        // Create a symbolic link called 'latest' to the new directory. If it already exists then delete it first
        $strLatestDir = dirname($strResortDataDir) . '/latest';
        if (file_exists($strLatestDir)) {
            unlink($strLatestDir);
        }
        symlink($strResortDataDir, $strLatestDir);

        // Make sure the summary file starts with 00, so it's the first one in the directory
        $strResortDataFile = $strResortDataDir . '/00-ResortData.csv';

        // Now write the data to the file
        $fp = fopen($strResortDataFile, 'w');
        $headerDone = false;
        foreach ($arrResortData as $item) {
            if (!$headerDone) {
                $itemHeader = $item->getHeader();
                fwrite($fp, $itemHeader);
                fwrite($fp, "\n");
                $headerDone = true;
            }
            $itemValues = $item->getValues();
            fwrite($fp, $itemValues);
            fwrite($fp, "\n");
        }
        fclose($fp);

        // For each Resort write the forecast data to a file
        foreach ($arrResortData as $resort) {
            $resortName = $resort->strResortName;
            // Remove any non-alphanumeric characters from the resort name
            $resortName = preg_replace('/[^A-Za-z0-9\-]/', '', $resortName);
            $strResortForecastFile = $strResortDataDir . '/' . $resortName . '-Forecast.csv';
            $resort->saveForecastToCSV($strResortForecastFile);
        }
    }

    /**
     * Static function to write resort and forecast data to the database.
     *
     * @param array $arrResortData
     * @param mysqli $conn
     * @return void
     * @throws Exception
     */
    public static function writeDataToDB(array $arrResortData, mysqli $conn): void
    {
        logger()->debug("writeResortDataToDB: Writing resort data to database",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        /** @noinspection SqlWithoutWhere */
        $strSQL = "DELETE FROM scgb_resort_data;";
        $stmt = $conn->prepare($strSQL);
        $stmt->execute();

        $strSQL = "INSERT INTO scgb_resort_data (" .
            "intSCGBResortID, " .
            "dtmLastUpdate, " .
            "strResortName, " .
            "strResortCountry, " .
            "fltMinTemperature, " .
            "fltMaxTemperature, " .
            "strWeather, " .
            "strWeatherURL, " .
            "intSnowLowerPisteInCentimeters, " .
            "intSnowUpperPisteInCentimeters, " .
            "intLowerWeatherStationElevation, " .
            "intUpperWeatherStationElevation, " .
            "dtmLastSnowed, " .
            "fltSnowForecastNext3DaysInCentimeters, " .
            "fltSnowForecastNext6DaysInCentimeters, " .
            "strOffPisteSnowConditions, " .
            "intLiftsOpen, " .
            "intLiftsTotal, " .
            "dtmResortOpening, " .
            "dtmResortClosing" .
            ") VALUES (%d, now(), '%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)";

        foreach ($arrResortData as $resortData) {
            // We need to escape possible ' in the resort name
            $strSQLStatement = sprintf(
                $strSQL,
                $resortData->intSCGBResortID,
                str_replace("'", "\'", $resortData->strResortName),
                $resortData->strResortCountry,
                ResortData::getSQLValue($resortData->fltMinTemperature),
                ResortData::getSQLValue($resortData->fltMaxTemperature),
                ResortData::getSQLValue($resortData->strWeather),
                ResortData::getSQLValue($resortData->strWeatherURL),
                ResortData::getSQLValue($resortData->intSnowLowerPisteInCentimeters),
                ResortData::getSQLValue($resortData->intSnowUpperPisteInCentimeters),
                ResortData::getSQLValue($resortData->intResortAltitude),
                ResortData::getSQLValue($resortData->intMountainAltitude),
                ResortData::getSQLValue($resortData->dtmLastSnowed),
                ResortData::getSQLValue($resortData->fltSnowForecastNext3DaysInCentimeters),
                ResortData::getSQLValue($resortData->fltSnowForecastNext6DaysInCentimeters),
                ResortData::getSQLValue($resortData->strOffPisteSnowConditions),
                ResortData::getSQLValue($resortData->intLiftsOpen),
                ResortData::getSQLValue($resortData->intLiftsTotal),
                ResortData::getSQLValue($resortData->dtmResortOpening),
                ResortData::getSQLValue($resortData->dtmResortClosing)
            );
            logger()->debug("writeResortDataToDB: SQL Statement: " . $strSQLStatement,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );

            $stmt = $conn->prepare($strSQLStatement);
            $stmt->execute();

            // Now write the forecast data to the database
            Forecast_Base::writeDataToDB(
                $resortData->arrForecasts,
                $resortData->intSCGBResortID,
                $resortData->strResortName,
                $resortData->strResortCountry,
                $conn);
        }
    }

    /**
     * Calculate the total count of forecasts across all resorts.
     *
     * @param array $arrResortData
     * @return int - the count of forecasts across all resorts
     */
    public static function getForecastCount(array $arrResortData): int
    {
        $intForecastCount = 0;
        foreach ($arrResortData as $resort) {
            $intForecastCount += count($resort->arrForecasts);
        }
        return $intForecastCount;
    }

    /**
     * Called for every forecast for this resort. Determine whether 3-day or 6-day forecast need to be updated.
     *
     * @param DateTime $now
     * @param DateTime $periodEnd
     * @param float $freshSnowfallInCentimeters
     * @return void
     */
    private function addSnow(DateTime $now, DateTime $periodEnd, float $freshSnowfallInCentimeters): void
    {
        $dayDifference = $now->diff($periodEnd)->days;
        if ($dayDifference <= 3) {
            $this->fltSnowForecastNext3DaysInCentimeters += $freshSnowfallInCentimeters;
        }
        if ($dayDifference <= 6) {
            $this->fltSnowForecastNext6DaysInCentimeters += $freshSnowfallInCentimeters;
        }
    }

    /**
     * Create Comma separated list of all the properties.
     * @return string
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function getHeader(): string
    {
        $headers = array(
            'intSCGBResortID',
            'strResortName',
            'strResortCountry',
            'fltMinTemperature',
            'fltMaxTemperature',
            'strWeather',
            'intSnowLowerPisteInCentimeters',
            'intSnowUpperPisteInCentimeters',
            'intResortAltitude',
            'intMountainAltitude',
            'fltAmountOfLastSnow',
            'dtmLastSnowed',
            'fltSnowForecastNext3DaysInCentimeters',
            'fltSnowForecastNext6DaysInCentimeters',
            'strOffPisteSnowConditions',
            'intLiftsOpen',
            'intLiftsTotal',
            'dtmResortOpening',
            'dtmResortClosing',
            'fltResortLongitude',
            'fltResortLatitude',
            'fltMountainLongitude',
            'fltMountainLatitude',
            'intResortSkiServiceID',
            'strSkiServiceResortName',
        );

        return implode(',', $headers);
    }

    /**
     * Create Comma separated list of values for this object.
     *
     * @return string
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function getValues(): string
    {
        $strResortClosing = '';
        $strResortOpening = '';
        $strLastSnowed = '';

        $dtmReportClosing = $this->dtmResortClosing;
        if ($dtmReportClosing != null) {
            $strResortClosing = $dtmReportClosing->format('Y-m-d');
        }

        $dtmReportOpening = $this->dtmResortOpening;
        if ($dtmReportOpening != null) {
            $strResortOpening = $dtmReportOpening->format('Y-m-d');
        }
        $dtmLastSnowed = $this->dtmLastSnowed;
        if ($dtmLastSnowed != null) {
            $strLastSnowed = $dtmLastSnowed->format('Y-m-d');
        }

        $values = array(
            $this->intSCGBResortID,
            $this->strResortName,
            $this->strResortCountry,
            $this->fltMinTemperature,
            $this->fltMaxTemperature,
            $this->strWeather,
            $this->intSnowLowerPisteInCentimeters,
            $this->intSnowUpperPisteInCentimeters,
            $this->intResortAltitude,
            $this->intMountainAltitude,
            $strLastSnowed,
            $this->fltSnowForecastNext3DaysInCentimeters,
            $this->fltSnowForecastNext6DaysInCentimeters,
            $this->strOffPisteSnowConditions,
            $this->intLiftsOpen,
            $this->intLiftsTotal,
            $strResortOpening,
            $strResortClosing,
            $this->fltResortLongitude,
            $this->fltResortLatitude,
            $this->fltMountainLongitude,
            $this->fltMountainLatitude,
            $this->intResortSkiServiceID,
            $this->strSkiServiceResortName,
        );

        return implode(',', $values);
    }

    /**
     * Dumps a forecast to a CSV file.
     *
     * @param string $strResortForecastFile - file to save the forecast to
     * @return void
     * @noinspection PhpUnusedPrivateMethodInspection
     */
    private function saveForecastToCSV(string $strResortForecastFile): void
    {
        logger()->debug(
            "Saving forecast data to CSV -> $strResortForecastFile",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // Open the file for writing
        $fp = fopen($strResortForecastFile, 'w');
        $headerWritten = false;
        foreach ($this->arrForecasts as $forecast) {
            if (!$headerWritten) {
                fwrite($fp, $forecast->getCSVHeader());
                $headerWritten = true;
            }
            fwrite($fp, $forecast->getCSVRow());
        }
        fclose($fp);
    }

    /**
     *  function to check the validity of the data in the object
     * - Mandatory fields present
     * - Fields of the correct type
     * - Fields within the correct range
     * - Duplicate of an existing record?
     *
     * @return void
     * @throws Exception
     */
    private function checkDataValidity(): void
    {
        // check $intSCGBResortID is positive and not a dupe
        if ($this->intSCGBResortID <= 0) {
            throw new Exception("Invalid SCGBResortID " . $this->intSCGBResortID . "/" . $this->strResortName);
        }
        if (array_key_exists($this->intSCGBResortID, self::$arrResortDataByID)) {
            throw new Exception("Duplicate SCGBResortID " . $this->intSCGBResortID . "/" . $this->strResortName);
        }
        self::$arrResortDataByID[$this->intSCGBResortID] = $this;

        if ($this->strResortName == '') {
            throw new Exception("Invalid Resort Name " . $this->strResortName);
        }
        if (array_key_exists($this->strResortName, self::$arrResortDataByName)) {
            throw new Exception("Duplicate Resort Name " . $this->strResortName);
        }
        self::$arrResortDataByName[$this->strResortName] = $this   ;

        if ($this->strResortCountry == '') {
            throw new Exception("Invalid Resort Country " . $this->strResortName . "/" . $this->strResortCountry);
        }
        // check whether $strResortCountry is in the list of valid countries. Was mandatory field,
        // but it's not used by website now, so we don't need to throw an exception if it's invalid
        if (!in_array($this->strResortCountry, self::ARR_VALID_COUNTRY_CODES)) {
            logger()->warning(
                "Invalid Resort Country",
                array(
                    'intSCGBResortID' => $this->intSCGBResortID,
                    'resort' => $this->strResortName,
                    'country' => $this->strResortCountry,
                    'file' => basename(__FILE__),
                    'function' => __FUNCTION__,
                    'line' => __LINE__,
                )
            );
        }

        // Check Latitude and Longitude are valid
        if ($this->fltResortLatitude != null && ($this->fltResortLatitude < -90 || $this->fltResortLatitude > 90)) {
            throw new Exception("Invalid Resort Latitude -> " . $this->strResortName . "/" . $this->fltResortLatitude);
        }
        if ($this->fltResortLongitude != null && ($this->fltResortLongitude < -180 || $this->fltResortLongitude > 180)) {
            throw new Exception("Invalid Resort Longitude " . $this->strResortName . "/" . $this->fltResortLongitude);
        }
        if ($this->fltMountainLatitude != null && ($this->fltMountainLatitude < -90 || $this->fltMountainLatitude > 90)) {
            throw new Exception("Invalid Mountain Latitude " . $this->strResortName . "/" . $this->fltMountainLatitude);
        }
        if ($this->fltMountainLongitude != null && ($this->fltMountainLongitude < -180 || $this->fltMountainLongitude > 180)) {
            throw new Exception("Invalid Mountain Longitude " . $this->strResortName . "/" . $this->fltMountainLongitude);
        }

        // check the $dtmResortClosing is after $dtmResortOpening
        if ($this->dtmResortClosing != null && $this->dtmResortOpening != null) {
            if ($this->dtmResortClosing < $this->dtmResortOpening) {
                throw new Exception("Resort Closing Date is before Resort Opening Date " . $this->strResortName);
            }
        }
    }

    /**
     * Extract a names value which can be used in a SQL statement.
     *
     * @param array $row
     * @param string $key
     * @param string $type
     * @param bool $mandatory
     * @return string|int|float|null
     * @throws Exception
     */
    private static function getrowvalue(
        array  $row,
        string $key,
        string $type,
        bool   $mandatory
    ): string|int|float|null
    {

        if (array_key_exists($key, $row)) {
            $retVal = $row[$key];
            if ($retVal == '' || $retVal == '-') {
                $retVal = null;
            }
        } else {
            if ($mandatory) {
                throw new Exception("Key " . $key . " not found in row");
            } else {
                $retVal = null;
            }
        }

        // if $retVal is not null then check it is of the correct type - if not return NULL
        if ($retVal) {
            if ($type == 'string') {
                if (!is_string($retVal)) {
                    $retVal = null;
                }
            } elseif ($type == 'int') {
                try {
                    $retVal = intval($retVal);
                } catch (Exception) {
                    $retVal = null;
                }
            } elseif ($type == 'float') {
                try {
                    $retVal = floatval($retVal);
                } catch (Exception) {
                    $retVal = null;
                }
            } else {
                throw new Exception("Unknown type " . $type);
            }
        }
        return $retVal;
    }

    /**
     * Static function to add elevation data to the resort data.
     *
     * @param array $arrResortData - array of ResortData objects
     * @return void
     * @throws Exception
     */
    private static function addElevationData(array $arrResortData): void
    {
        logger()->debug(
            "Adding elevation data to resort data",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        // Loop through the resorts and add the elevation data - we have to iterate twice as we want to avoid making
        // multiple calls to the API
        foreach ($arrResortData as $resortData) {
            ElevationData::checkLatLong($resortData->fltResortLatitude, $resortData->fltResortLongitude);
            ElevationData::checkLatLong($resortData->fltMountainLatitude, $resortData->fltMountainLongitude);
        }

        ElevationData::getMissingData();

        // Now add the elevation data to the resort data
        foreach ($arrResortData as $resortData) {
            $resortData->intResortAltitude = ElevationData::getElevation(
                $resortData->fltResortLatitude,
                $resortData->fltResortLongitude
            );
            $resortData->intMountainAltitude = ElevationData::getElevation(
                $resortData->fltMountainLatitude,
                $resortData->fltMountainLongitude
            );
        }
    }

    /**
     * Convert the passed parameter to something suitable for insertion in the database.
     * If passed value is null then return NULL, else return the value surrounded by single quotes if appropriate
     *
     * @param mixed $value - the value to be converted
     * @return string
     */
    private static function getSQLValue(mixed $value): string
    {
        if ($value === null) {
            return 'NULL';
        } else {
            if (is_numeric($value)) {
                return strval($value);
            } elseif (is_a($value, 'DateTime')) {
                return "'" . $value->format('Y-m-d H:i:s') . "'";
            } else {
                return "'" . $value . "'";
            }
        }
    }
}
