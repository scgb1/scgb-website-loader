<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use mysqli;

/**
 * Contains generic utilities used by other parts of the code.
 *
 * Generic utilities are:
 * - logger() - returns the global logging variable
 * - curl - takes an optional token and calls the URL specified
 * - getTimeStampedFilename - takes the passed 'file' and prepends a timestamp on the basename
 * - setupConfig - part of the initialisation. Load the config file and then processes command line overrides
 * - getConfigItem - returns the config item specified
 * - getDataFileName - returns a config item which is file name as an absolute path (if not already absolute)
 * - getDBConnection - returns a connection to the database
 */

use Monolog\Logger;
use Monolog\Level;
use Monolog\Handler\Handler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\NativeMailerHandler;
use Monolog\Handler\BufferHandler;
use Monolog\Formatter\LineFormatter;

/**
 * Initialise the application. Setup logging, read the config file and process command line overrides.
 *
 * If an email address is specified in the config file, then an email handler is added to the logger. This will only
 * be used for ERROR level messages.
 *
 * @return Handler|null - the email handler if created, otherwise null. This used to flush at the end
 */
function initialise() : Handler | null
{
    // Basic initialisation, including setting up logging. Until this is completed, the code can only exit
    try {
        // Change default error handling to exit on warnings.
        set_error_handler(function ($errno, $errstr, $errfile, $errline) {
            // error suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }
            logger()->emergency(
                $errstr,
                array(
                    'file' => $errfile,
                    'line' => $errline,
                )
            );
            die(1);
        });

        // Read the config file and factor in overrides from the command line
        setupConfig();

        // Setup Logging. Start with logging to stdout. Logfile creation is handled by a shell script
        $handler = new StreamHandler("php://stdout", Level::fromName(getConfigItem('logLevel')));
        // the last “true” here tells it to remove empty [] from the end of the line
        $formatter = new LineFormatter(null, null, false, true);
        $handler->setFormatter($formatter);
        logger()->pushHandler($handler);
        $bulkHandler = null;

        // Add an email handler for ERROR (serious issues which should not occur)
        if ($errorEmail = getConfigItem('errorEmail', false)) {
            $handler = new NativeMailerHandler(
                @$errorEmail,
                'SkiClub Website Data Loader - Error',
                'website@skiclub.co.uk',
                Level::Error
            );

            $bulkHandler = new BufferHandler($handler);
            $bulkHandler->setFormatter($formatter);
            logger()->pushHandler($bulkHandler);
        }

        logger()->debug(
            'Config: ' . print_r($GLOBALS['config'], true),
            array(
                'file' => basename(__FILE__),
                'line' => __LINE__,
            )
        );

    } catch (Exception $e) {
        print('Failed to perform basic initialisation - Cannot continue: ' . $e->getMessage() . "\n");
        die(1);
    }
    return $bulkHandler;
}

/**
 * function curl - call the passed URL with a login token if necessary.
 *
 * @param $url
 * @param null $token - optional login token to add to the call
 * @return string
 * @throws Exception
 */
function curl($url, $token = null) : string
{
    logger()->debug('curl URL: ' . $url,
        array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_FAILONERROR => true,
        )
    );

    if ($token != null) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $token
        ));
    }

    // If we get an empty response then sleep for 5 seconds and try again
    // retry up to 5 times
    for ($i=0; $i<5; $i++) {
        $response = curl_exec($curl);
        if ($response == '') {
            $sleepTime = 5 * ($i+1);
            logger()->warning(
                'Empty Response - retrying',
                array(
                    'sleepTime' => $sleepTime,
                    'file' => basename(__FILE__),
                    'function' => __FUNCTION__,
                    'line' => __LINE__,
                )
            );
            sleep($sleepTime);
        } else {
            break;
        }
    }
    if (curl_errno($curl)) {
        $error_msg = curl_error($curl);
        logger()->emergency('curl error: ' . curl_errno($curl) . "/" .$error_msg,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
        throw new Exception('curl error: ' . curl_errno($curl) . '/' .$error_msg);
    }

    curl_close($curl);

    return $response;
}

/**
 * Returns a timestamped filename and creates the target directory if it doesn't exist.
 *
 * @param string $basename - the filename to timestamp
 * @return string
 */
function getTimeStampedFilename(string $basename) : string
{
    $dirname = dirname($basename);
    $filename = basename($basename);
    if (!file_exists($dirname)) {
        mkdir($dirname, 0700, true);
    }

    // Add a timestamp to filename
    $filename = date('Ymd.His') . '-' . $filename;
    return($dirname . '/' . $filename);
}

function logger()
{
    // If the logger not already set up, do it now
    if (!isset($GLOBALS['log'])) {
        $GLOBALS['log'] = new Logger('SCGB');
    }
    return $GLOBALS['log'];
}

/**
 * Get the default config items (from config.php) and override with any set on the command line.
 *
 * @return void
 */
function setupConfig() : void
{
    $configFile = __DIR__ . "/" . '../config.php';

    // Check the config file exists
    if (!file_exists($configFile)) {
        print("Config file not found: " . $configFile . ".  Can't continue\n");
        die(1);
    }

    $GLOBALS['config'] = include __DIR__ . "/" . '../config.php';

    $argc = $_SERVER['argc'];
    $argv = $_SERVER['argv'];

    // Now iterated through the command line options and override any config items
    for ($i=1; $i<$argc; $i++) {
        $option = $argv[$i];
        if (str_starts_with($option, '--')) {
            $option = substr($option, 2);
            $optionParts = explode('=', $option);
            $optionName = $optionParts[0];
            $optionValue = $optionParts[1];
            if ($optionValue == "") {
                $optionValue = null;
            }
            $GLOBALS['config'][$optionName] = $optionValue;
        }
    }
}

/**
 * Function to get a config item and handle missing ones gracefully.
 *
 * All options can be specified or overridden from the command line
 *
 * @throws Exception
 * @param string $configItem - the name of the config item to retrieve
 * @param bool $mandatory - whether the config item is mandatory or not. Defaults true
 */
function getConfigItem(string $configItem, bool $mandatory = true)
{
    $config = $GLOBALS['config'];
    if (isset($config[$configItem])) {
        return $config[$configItem];
    } else {
        if (!$mandatory) {
            return null;
        }
        $GLOBALS['log']->error('Missing config item: ' . $configItem, array('process' => basename(__FILE__)));
        throw new Exception('Missing config item: ' . $configItem);
    }
}

/**
 * Function to get a config item which is a file name and return it as an absolute path.
 *
 * @param string $configItem - the name of the config item to retrieve
 * @param bool $mandatory - whether the config item is mandatory or not. Defaults true
 * @return string|null
 * @throws Exception
 */
function getDataFileName(string $configItem, bool $mandatory = true) : string|null
{
    $filename = getConfigItem($configItem, $mandatory);
    if ($filename != null) {
        // If $filename doesn't start with a / then it is relative to the data directory
        if (!str_starts_with($filename, '/')) {
            $filename = dirname($_SERVER['argv'][0]) . "/" . $filename;
        }
    }
    return $filename;
}

/**
 * Function to get a database connection.
 *
 * @return mysqli
 * @throws Exception
 */
function getDBConnection() : mysqli
{
    $servername = getConfigItem('databaseServer');
    $username = getConfigItem('databaseUser');
    $password = getConfigItem('databasePassword');
    $db = getConfigItem('databaseName');

    // Connect to the database
    logger()->debug(
        'Connecting to database',
        array(
            'databaseServer' => $servername,
            'databaseName' => $db,
            'databaseUser' => $username,
            'databasePassword' => 'SECRET',
            'file' => basename(__FILE__),
            'function' => __FUNCTION__,
            'line' => __LINE__,
        )
    );

    $conn = new mysqli($servername, $username, $password);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $conn->select_db($db);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

/**
 * Confirm that the passed value is of the type specified else return null.
 *
 * @param mixed $value
 * @param string $type
 * @return string|int|null|DateTime
 */
function checkType(mixed $value, string $type): string|int|null|DateTime
{
    if (is_array($value)) {
        return null;
    }
    if ($type == 'int') {
        try {
            return intval($value);
        } catch (Exception) {
            return null;
        }
    } elseif ($type == 'string') {
        if (is_string($value)) {
            return $value;
        } else {
            return null;
        }
    } elseif ($type == 'date') {
        try {
            return new DateTime($value);
        } catch (Exception) {
            return null;
        }
    } else {
        return null;
    }
}

/**
 * returns a date as long as the parameter is a string or a DateTime object
 *
 * @param $dtm - the date time to be checked
 * @throws Exception
 */
function getDateTime($dtm): DateTime|null
{
    if (is_string($dtm)) {
        return new DateTime($dtm);
    } elseif (is_object($dtm) && get_class($dtm) == 'DateTime') {
        return $dtm;
    } else {
        return null;
    }
}
