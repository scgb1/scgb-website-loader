<?php

namespace SCGB;

use Exception;

class Forecast_DTN extends Forecast_Base
{
    /**
     * @inheritDoc
     */
    static public function getForecastsForLocation(array $arrForecastsFromProvider): array
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @return string
     * @throws Exception
     */
    static public function getAuthURL(): string
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @param string|null $period
     * @return string
     * @throws Exception
     */
    static public function getWeatherCodes(string $period = null): string
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    static public function getForecastURLs(float $latitude, float $longitude): array
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    protected function setWeather(): void
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }

    /**
     * @inheritDoc
     */
    protected function setWeatherURL(): void
    {
        //NOOP - not implemented
        throw new Exception("shouldn't be here!");
    }
}