<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

require_once (__DIR__ . '/Utils.php');
require_once (__DIR__ . '/Collector_Base.php');
require_once (__DIR__ . '/Forecast_Base.php');
require_once(__DIR__ . '/Forecast_DTN_Legacy.php');
require_once (__DIR__ . '/Forecast_DTN_V1.php');

/**
 * Container used to fetch forecast data from CollectorDTN
 */
class Collector_DTN extends Collector_Base
{
    private bool $useV1API = false;
    private ?string $dtnToken = null;

    /**
     * Constructor.
     *
     * Depending on whether:
     * - using legacy or V1
     * - saving or loading data from a file
     * Set the appropriate properties and collect a CollectorDTN token if necessary
     *
     * @throws Exception
     */
    public function __construct() {
        // legacy or V1 API
        $strAPIVersion = 'Legacy';
        if (getConfigItem('weatherClientAudience', false) !== null) {
            $this->useV1API = true;
            $strAPIVersion = 'V1';
        }

        logger()->debug("Using CollectorDTN API Version $strAPIVersion",
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__, ));

        parent::__construct($strAPIVersion);

        // If not loading from files then get the CollectorDTN Token
        if ($this->loadForecastDataFromDir === null) {
            logger()->debug("Loading data from CollectorDTN - Collect a CollectorDTN Token",
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            $this->dtnToken = Collector_DTN::getDTNLogonToken();
        }
        return $this;
    }

    /**
     * Get a forecast for a location from CollectorDTN.
     *
     * To maintain compatibility with the new API only we only request 1 location at a time.
     * The old API allowed multiple locations to be requested in a single call!
     * Also, because not all parameters available in the same call we may make multiple calls to get all the data
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    public function getForecastForLocation(float $latitude, float $longitude): array
    {
        logger()->debug('Getting Forecast from CollectorDTN for ' . $latitude . '/' . $longitude,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        if ($this->useV1API) {
            $forecast = new Forecast_DTN_V1();
        } else {
            $forecast = new Forecast_DTN_Legacy();
        }
        return $this->getForecastForLocationFromProvider($latitude, $longitude, $forecast, $this->dtnToken);
    }

    /**
     * Create a URL to fetch a forecast from CollectorDTN. This differs depending on the API version.
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    protected function getForecastURLs(float $latitude, float $longitude): array
    {
        if ($this->useV1API) {
            return Forecast_DTN_V1::getForecastURLs($latitude, $longitude);
        } else {
            return Forecast_DTN_Legacy::getForecastURLs($latitude, $longitude);
        }
    }

    /**
     * Get the CollectorDTN logon token - the V1 and legacy APIs have different methods of getting this
     *
     * @return string | null - the token or null if we didn't get it
     * @throws Exception
     */
    private function getDTNLogonToken(): string|null
    {
        logger()->debug('Getting CollectorDTN Logon Token',
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        $client_id = getConfigItem('weatherClientID');
        $client_secret = getConfigItem('weatherClientSecret');

        $ch = curl_init();

        // This the point where the code where we determine whether using the legacy or V1 API
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);

        if ($this->useV1API) {
            $client_audience = getConfigItem('weatherClientAudience');
            curl_setopt($ch, CURLOPT_URL, Forecast_DTN_V1::getAuthURL());
            curl_setopt($ch, CURLOPT_ENCODING, '');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                '{
            "grant_type": "client_credentials",
            "client_id": "' . $client_id . '",
            "client_secret": "' . $client_secret . '",
            "audience": "' . $client_audience . '"
        }'
            );
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                )
            );
        } else {
            $comb = $client_id . ':' . $client_secret;
            curl_setopt($ch, CURLOPT_URL, Forecast_DTN_Legacy::getAuthURL());
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/x-www-form-urlencoded',
            ]);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $comb);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        }
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            logger()->error('Error getting CollectorDTN Logon Token: ' . curl_error($ch),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            return null;
        }

        $auth_string = json_decode($response, true); // token will be with in this json
        if ($this->useV1API) {
            $auth_string = $auth_string['data'];
        }
        logger()->debug("Successfully Connected to CollectorDTN -> " . var_export($auth_string, true),
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

        return ($auth_string["access_token"]);
    }
}
