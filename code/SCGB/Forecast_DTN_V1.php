<?php
declare(strict_types=1);

namespace SCGB;

use DateInterval;
use DateTime;
use Exception;

require_once(__DIR__ . '/Forecast_DTN.php');

/**
 * Implements the WeatherForecastBase class for the CollectorDTN V1 API.
 */
class Forecast_DTN_V1 extends Forecast_DTN
{
    const WEATHER_AUTH_URL = 'https://api.auth.dtn.com/v1/tokens/authorize';
    const WEATHER_FORECAST_URL =
        'https://weather.api.dtn.com/v1/conditions?interval=hour&lat=%f4&lon=%f&startTime=%s&endTime=%s&parameters=%s';// Define which values come from which forecast
    const  ARR_DTN_WEATHER_CODES = array(
        'airTempLowerBound',
        'airTempUpperBound',
        'feelsLikeTemp',
        'cloudCover',
        'precipType',
        'snowAccPeriod',
        'windSpeed',
        'windDirection',
        'descriptors',
    );

    protected float $airTempLowerBound;
    protected float $airTempUpperBound;
    protected float $feelsLikeTemp;
    protected int $cloudCover;
    protected int $precipType;
    protected float $snowAccPeriod;
    protected float $windSpeed;
    protected ?float $windDirection = null;
    protected int $cloud_cover_descriptor_code;
    protected string $cloud_cover_descriptor_icon;
    protected string $cloud_cover_descriptor_text;
    protected int $precipitation_descriptor_code;
    protected string $precipitation_descriptor_icon;
    protected string $precipitation_descriptor_text;
    protected int $visibility_obstruction_descriptor_code;
    protected string $visibility_obstruction_descriptor_icon;
    protected string $visibility_obstruction_descriptor_text;
    protected int $weather_descriptor_code;
    protected string $weather_descriptor_icon;
    protected string $weather_descriptor_text;
    protected int $wind_direction_descriptor_code;
    protected string $wind_direction_descriptor_icon;
    protected string $wind_direction_descriptor_text;

    /**
     * Constructor takes an array of data from CollectorDTN.
     *
     * Sample data returned by the CollectorDTN API.
     *
     * 2023-07-25T18:00:00Z = {array[9]}
     *      airTempLowerBound = {float} 6.1
     *      airTempUpperBound = {float} 9.4
     *      feelsLikeTemperature = {float} 6.4
     *      cloudCover = {int} 84
     *      precipType = {int} 2
     *      snowAccPeriod = {int} 0
     *      windSpeed = {float} 2.7
     *      windDirection = {int} 311
     *      descriptors = {array[5]}
     *       cloud_cover_descriptor = {array[3]}
     *        code = {int} 20400
     *        icon = "http://icons.clearapis.com/en-us/style1/v1/128/cover_mostlycloudyday.png"
     *        text = "Mostly Cloudy"
     *       precipitation_descriptor = {array[3]}
     *        code = {int} 60205
     *        icon = "http://icons.clearapis.com/en-us/style1/v1/128/precip_sprinkles.png"
     *        text = "Chance of Sprinkles"
     *       visibility_obstruction_descriptor = {array[3]}
     *        code = {int} 30000
     *        icon = "http://icons.clearapis.com/en-us/style1/v1/128/visibility_hazeday.png"
     *        text = "Haze"
     *       weather_descriptor = {array[3]}
     *        code = {int} 30000
     *        icon = "http://icons.clearapis.com/en-us/style1/v1/128/visibility_hazeday.png"
     *        text = "Haze"
     *       wind_direction_descriptor = {array[3]}
     *        code = {int} 52000
     *        icon = "http://icons.clearapis.com/en-us/style1/v1/128/direction_nw.png"
     *        text = "Northwest"
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        return $this;
    }

    /**
     * @param array $arrForecast
     * @return void
     * @throws Exception
     */
    public function setForecastProperties(array $arrForecast): void
    {
        $strForecastDate = $arrForecast['date'];
        $latitude = $arrForecast['latitude'];
        $longitude = $arrForecast['longitude'];
        $dtmForecastDate = new DateTime($strForecastDate);
        $this->airTempLowerBound = $arrForecast['airTempLowerBound'];
        $this->airTempUpperBound = $arrForecast['airTempUpperBound'];
        $this->feelsLikeTemp = $arrForecast['feelsLikeTemp'];
        $this->cloudCover = intval($arrForecast['cloudCover']);
        $this->precipType = $arrForecast['precipType'];
        $this->snowAccPeriod = $arrForecast['snowAccPeriod'];
        $this->windSpeed = $arrForecast['windSpeed'];
        // Wind Direction is null if there is no wind
        $this->windDirection = $arrForecast['windDirection'];
        $this->cloud_cover_descriptor_code = $arrForecast['descriptors']['cloud_cover_descriptor']['code'];
        $this->cloud_cover_descriptor_icon = $arrForecast['descriptors']['cloud_cover_descriptor']['icon'];
        $this->cloud_cover_descriptor_text = $arrForecast['descriptors']['cloud_cover_descriptor']['text'];
        $this->precipitation_descriptor_code = $arrForecast['descriptors']['precipitation_descriptor']['code'];
        $this->precipitation_descriptor_icon = $arrForecast['descriptors']['precipitation_descriptor']['icon'];
        $this->precipitation_descriptor_text = $arrForecast['descriptors']['precipitation_descriptor']['text'];
        $this->visibility_obstruction_descriptor_code = $arrForecast['descriptors']['visibility_obstruction_descriptor']['code'];
        $this->visibility_obstruction_descriptor_icon = $arrForecast['descriptors']['visibility_obstruction_descriptor']['icon'];
        $this->visibility_obstruction_descriptor_text = $arrForecast['descriptors']['visibility_obstruction_descriptor']['text'];
        $this->weather_descriptor_code = $arrForecast['descriptors']['weather_descriptor']['code'];
        $this->weather_descriptor_icon = $arrForecast['descriptors']['weather_descriptor']['icon'];
        $this->weather_descriptor_text = $arrForecast['descriptors']['weather_descriptor']['text'];
        $this->wind_direction_descriptor_code = $arrForecast['descriptors']['wind_direction_descriptor']['code'];
        $this->wind_direction_descriptor_icon = $arrForecast['descriptors']['wind_direction_descriptor']['icon'];
        $this->wind_direction_descriptor_text = $arrForecast['descriptors']['wind_direction_descriptor']['text'];

        // Because they hourly forecasts, V1 doesn't supply an end date, so create one
        $dtmForecastEndDate = clone($dtmForecastDate);
        $dtmForecastEndDate->add(new DateInterval('PT1H'));

        // V1 Wind speed arrives in m/s so convert to mph
        $windSpeedMPH = intval($this->windSpeed * 2.23694);

        parent::setCoreProperties(
            $dtmForecastDate,
            $dtmForecastEndDate,
            $latitude,
            $longitude,
            $windSpeedMPH,
            intval($this->windDirection),
            $this->snowAccPeriod / 10, // Need to convert from mm to centimetres
            $this->airTempLowerBound,
            $this->airTempUpperBound,
            $this->feelsLikeTemp
        );
        $this->setWeather();
    }

    protected function setWeather(): void
    {
        $this->strWeather = $this->weather_descriptor_text;
    }

    /**
     * Uber constructor for the Forecast class. Takes the raw data from the CollectorDTN API and converts it into forecasts.
     *
     * @param array $arrForecastsFromProvider
     * @return array
     * @throws Exception
     */
    static public function getForecastsForLocation(array $arrForecastsFromProvider): array
    {
        if (!isset($arrForecastsFromProvider[0]['content']['items'][0])) {
            logger()->emergency(
                'Missing data from CollectorDTN',
                array(
                    '$arrForecastsFromDTN' => var_export($arrForecastsFromProvider, true),
                    'file' => basename(__FILE__),
                    'line' => __LINE__,
                    'function' => __FUNCTION__,
                )
            );
            throw new Exception('Missing content from CollectorDTN');
        }
        $forecastForHour = $arrForecastsFromProvider[0]['content']['items'][0];
        if (!isset($forecastForHour['parameters'])) {
            logger()->emergency(
                'Missing parameters from CollectorDTN',
                array(
                    '$arrForecastsFromDTN' => var_export($arrForecastsFromProvider, true),
                    'file' => basename(__FILE__),
                    'line' => __LINE__,
                    'function' => __FUNCTION__,
                )
            );
            throw new Exception('Missing parameters from CollectorDTN');
        }
        $arrDTNWeather = $forecastForHour['parameters'];
        $latitude = $forecastForHour['geospatial']['coordinates'][1];
        $longitude = $forecastForHour['geospatial']['coordinates'][0];

        // The data is returned in an inconvenient format with dates lower down in the array - we need to switch that
        $arrJSON = array();
        foreach ($arrDTNWeather as $parameter => $arrDates) {
            foreach ($arrDates as $date => $value) {
                $arrJSON[$date][$parameter] = $value;
            }
        }

        // Now turn that into an array of WeatherForecasts
        $arrForecasts = array();
        foreach ($arrJSON as $date => $forecast) {
            // Ignore old forecasts
            if (count($forecast) == 9)
            {
                $forecast['latitude'] = $latitude;
                $forecast['longitude'] = $longitude;
                $forecast['date'] = $date;
                $x = new Forecast_DTN_V1();
                $x->setForecastProperties($forecast);
                $arrForecasts[$date] = $x;
            }
        }
        return $arrForecasts;
    }

    static public function getAuthURL(): string
    {
        return self::WEATHER_AUTH_URL;
    }

    /**
     * Get the 2 URLS needed for the Legacy Weather Forecast API.
     *
     * 2 periods (hourly and 6 hourly), PT0S and PT6H combined with dates 10 days apart
     *
     * @param float $latitude
     * @param float $longitude
     * @return array
     * @throws Exception
     */
    static public function getForecastURLs(float $latitude, float $longitude): array
    {
        // Add dates in format 2023-06-25T00:00:00Z
        $validFrom = date('Y-m-d') . 'T00:00:00Z';

        // Add 10 days to the current date
        $validUntil = date('Y-m-d', strtotime('+10 days')) . 'T00:00:00Z';

        return array(sprintf(
            self::WEATHER_FORECAST_URL,
            $latitude,
            $longitude,
            $validFrom,
            $validUntil,
            Forecast_DTN_V1::getWeatherCodes()
        ));
    }

    static public function getWeatherCodes(string $period = null): string
    {
        return implode(',', self::ARR_DTN_WEATHER_CODES);
    }

    protected function setWeatherURL(): void
    {
        $this->strWeatherURL = $this->weather_descriptor_icon;
    }
}