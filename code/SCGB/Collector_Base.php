<?php
declare(strict_types=1);
namespace SCGB;

use Exception;

abstract class Collector_Base
{
    protected ?string $saveForecastDataToDir = null;
    protected ?string $loadForecastDataFromDir;

    /**
     * Constructor.
     *
     * Depending on whether:
     * - saving or loading data from a file
     * Set the appropriate properties
     *
     * @throws Exception
     */
    public function __construct(string $strAPIVersion)
    {
        // See whether saving and loading data from files or from CollectorDTN
        $this->loadForecastDataFromDir = getDataFileName('loadForecastDataFromDir', false);

        // No point saving the data if loading from files
        if ($this->loadForecastDataFromDir === null) {
            $this->saveForecastDataToDir = getDataFileName('saveForecastDataToDir', false);

            // Get the timestamped version of the filename
            if ($this->saveForecastDataToDir !== null) {
                $this->saveForecastDataToDir =
                    getTimeStampedFilename($this->saveForecastDataToDir) . "-" . $strAPIVersion;
            }
        }

        return $this;
    }

    public abstract function getForecastForLocation(float $latitude, float $longitude) : array  ;
    protected abstract function getForecastURLs(float $latitude, float $longitude): array;

    /**
     * Get a forecast for a location from CollectorDTN.
     *
     * To maintain compatibility with the new API only we only request 1 location at a time.
     *
     * @param float $latitude
     * @param float $longitude
     * @param Forecast_Base $forecast
     * @param null $token
     * @return array
     * @throws Exception
     */
    protected function getForecastForLocationFromProvider(
        float $latitude, float $longitude, Forecast_Base $forecast, $token = null) : array
    {
        // Determine whether to load from file or from CollectorDTN
        try {
            $jsonDTNweather = array();
            if ($this->loadForecastDataFromDir !== null) {
                $jsonDTNweather = $this->getForecastFromFile($latitude, $longitude);
            } else {
                // Get the forecast from CollectorDTN
                foreach ($this->getForecastURLs($latitude, $longitude) as $url) {
                    $strDTNWeather = curl($url, $token);
                    $jsonDTNweather[] = json_decode($strDTNWeather, true);

                    if ($jsonDTNweather === null) {
                        throw new Exception('Failed to get forecast from CollectorDTN');
                    }
                }

                if ($this->saveForecastDataToDir !== null) {
                    $this->saveForecastToFile($jsonDTNweather, $latitude, $longitude);
                }
            }

            $arrForecastsForLocation= $forecast::getForecastsForLocation($jsonDTNweather);

        } catch (Exception $e) {
            logger()->error(
                'Failed to get forecast from CollectorDTN: ' . $e->getMessage(),
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
            );
            throw new Exception('Failed to get forecast from CollectorDTN: ' . $e->getMessage());
        }
        return $arrForecastsForLocation;
    }

    /**
     * Load the json forecast data from a file and turn it into an array
     *
     * @param float $latitude
     * @param float $longitude
     * @return array|null
     */
    protected function getForecastFromFile(
        float  $latitude,
        float  $longitude
    ): array|null
    {
        $filePath = $this->loadForecastDataFromDir . "/" . $latitude . "." . $longitude . ".json";

        logger()->info(
            'Loading Forecast from file -> ' . $filePath,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );
        if (!file_exists($filePath)) {
            logger()->warning('Invalid filename for loading forecast -> ' . $filePath,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            return null;
        }

        $jsonDTNweather = file_get_contents($filePath);
        if ($jsonDTNweather === false) {
            logger()->error('Failed to load forecast from file -> ' . $filePath,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));
            return null;
        }
        return json_decode($jsonDTNweather, true);
    }

    /**
     * Save the forecast data to a file - used for dev purposes to allow us to test the forecast code without
     * having to hit the API.
     *
     * @param array $forecastsForLocation - the data to save - array of json strings
     * @param float $latitude
     * @param float $longitude
     * @throws Exception
     */
    protected function saveForecastToFile(
        array  $forecastsForLocation,
        float  $latitude,
        float  $longitude
    ): void
    {
        $filePath = $this->saveForecastDataToDir .
            "/" . $latitude . "." . $longitude . ".json";

        // Get the dirname of the file and create it if it doesn't exist
        $dirName = dirname($filePath);
        if (!is_dir($dirName)) {
            mkdir($dirName, 0755, true);
        }

        logger()->debug(
            'Saving forecast to -> ' . $filePath,
            array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,)
        );

        $objData = json_encode($forecastsForLocation);
        if (is_writable(dirname($filePath))) {
            $fp = fopen($filePath, "w");
            fwrite($fp, $objData);
            fclose($fp);
        } else {
            throw new Exception('Cannot write forecast to file -> ' . $filePath);
        }
    }
}