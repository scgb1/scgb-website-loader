<?php
declare(strict_types=1);
namespace SCGB;

use DateTime;
use Exception;
use mysqli;

/**
 * Base class for all weather forecast classes containing the data saved to the database and used by the website
 *
 * This class contains the core properties and methods common to all weather forecast classes
 */
abstract Class Forecast_Base
{
    /**
     * Identify the periods AM, PM and Night as used on the website.
     *
     * These always referenced in local time (they pick up timezone from the CollectorDTN data)
     */
    const AM = 9;
    const PM = 15;
    const NIGHT = 21;

    /**
     * String representations of the periods AM, PM and Night as used on the website.
     */
    const ARR_PERIOD_STRINGS = array(
        self::AM => 'AM',
        self::PM => 'PM',
        self::NIGHT => 'NIGHT',
    );
    /**
     * The base icon for a Northerly wind - we manipulate this to point in the right direction and add the wind speed
     */
    const WIND_BASE_IMAGE = 'wind-base.png';
    /**
     * Textual representation of the wind direction
     */
    const WIND_DIRECTIONS = array(
        0 => 'N',
        1 => 'NE',
        2 => 'E',
        3 => 'SE',
        4 => 'S',
        5 => 'SW',
        6 => 'W',
        7 => 'NW',
    );

    /**
     * Core properties - the ones that get written to the database regardless of the version of the API
     */
    private float $latitude;
    private float $longitude;
    protected DateTime $dtmForecastDate;
    protected DateTime $dtmForecastEndDate;
    protected ?string $strForecastPeriod = null;
    protected ?string $strWeather = null;
    protected string $strWeatherURL = '';
    protected int $intWindSpeedInMPH;
    protected int $intWindDirectionInDegrees;
    protected ?string $strWindDirection = null;
    protected ?string $strWindURL = null;
    protected float $fltSnowExpectedinCentimeters;
    protected float $fltMinAirTemperature;
    protected float $fltMaxAirTemperature;
    protected float $fltFeelsLikeTemperature;
    protected ?bool $blnIsTopForecast = null;

    /**
     * @param array $arrForecastsFromProvider
     * @return array
     * @throws Exception
     */
    abstract static public function getForecastsForLocation(array $arrForecastsFromProvider) : array;

    /**
     * Get the URLs to use to get the weather forecast from Collector.
     *
     * @param float $latitude
     * @param float $longitude
     * @return array - this needs to be an array as the legacy API requires us to call it twice
     * @throws Exception
     */
    abstract static public function getForecastURLs(float $latitude, float $longitude) : array;

    /**
     * Called after the constructor to set the weather properties
     *
     * @return void
     * @throws Exception
     */
    abstract protected function setWeather() : void;

    /**
     * Called after the constructor to set the weather URL
     *
     * @return void
     * @throws Exception
     */
    abstract protected function setWeatherURL() : void;

    /**
     * Base Class constructor
     *
     * @throws Exception
     */
    public function __construct()
    {
        return $this;
    }

    /**
     * @param DateTime $dtmForecastDate
     * @param DateTime $dtmForecastEndDate
     * @param float $latitude
     * @param float $longitude
     * @param int $intWindSpeedInMPH
     * @param int $intWindDirectionInDegrees
     * @param float $fltSnowExpectedinCentimeters
     * @param float $fltMinAirTemperature
     * @param float $fltMaxAirTemperature
     * @param float $fltFeelsLikeTemperature
     * @return void
     * @throws Exception
     */

    protected function setCoreProperties(
        DateTime $dtmForecastDate,
        DateTime $dtmForecastEndDate,
        float $latitude,
        float $longitude,
        int $intWindSpeedInMPH,
        int $intWindDirectionInDegrees,
        float $fltSnowExpectedinCentimeters,
        float $fltMinAirTemperature,
        float $fltMaxAirTemperature,
        float $fltFeelsLikeTemperature
    ) : void {
        $this->dtmForecastDate = $dtmForecastDate;
        $this->dtmForecastEndDate = $dtmForecastEndDate;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->intWindSpeedInMPH = $intWindSpeedInMPH;
        $this->intWindDirectionInDegrees = $intWindDirectionInDegrees;
        $this->fltSnowExpectedinCentimeters = $fltSnowExpectedinCentimeters;
        $this->fltMinAirTemperature = $fltMinAirTemperature;
        $this->fltMaxAirTemperature = $fltMaxAirTemperature;
        $this->fltFeelsLikeTemperature = $fltFeelsLikeTemperature;

        $this->convertWindToStr();
        $this->setWindURL();
        $this->setWeatherURL();
    }
    // Getters

    /** @noinspection PhpUnused */
    public function getForecastDate() : DateTime
    {
        return $this->dtmForecastDate;
    }

    /**
     * @return DateTime
     * @noinspection PhpUnused
     */
    public function getForecastEndDate(): DateTime
    {
        return $this->dtmForecastEndDate;
    }

    /** @noinspection PhpUnused */
    public function getSnowExpectedInCentimeters() : float
    {
        return $this->fltSnowExpectedinCentimeters;
    }

    /** @noinspection PhpUnused */
    public function getMinTemperature() : float
    {
        return $this->fltMinAirTemperature;
    }

    /** @noinspection PhpUnused */
    public function getMaxTemperature() : float
    {
        return $this->fltMaxAirTemperature;
    }

    /** @noinspection PhpUnused */
    public function getWeather() : string
    {
        return $this->strWeather;
    }

    /** @noinspection PhpUnused */
    public function getWeatherURL() : string
    {
        return $this->strWeatherURL;
    }

    /**
     * @return string|null
     * @noinspection PhpUnused
     */
    public function getForecastPeriod() : string | null
    {
        return $this->strForecastPeriod;
    }

    /**
     * @param bool $blnIsTopForecast
     * @return void
     * @noinspection PhpUnused
     */
    public function setIsTopForecast(bool $blnIsTopForecast) : void
    {
        $this->blnIsTopForecast = $blnIsTopForecast;
    }

    /**
     * Convert the Wind direction in Degrees to N,S,E,W, NE, NW, SE, SW but only if the wind speed is greater than 1mph.
     * If the wind speed is less than 1mph then set the wind direction to an empty string.
     *
     * @return void
     * @throws Exception
     */
    private function convertWindToStr() : void
    {
        if ($this->intWindSpeedInMPH <= 1) {
            $this->strWindDirection = '';
        } elseif ($this->intWindDirectionInDegrees <0 || $this->intWindDirectionInDegrees > 360) {
            throw new Exception('Wind direction in degrees is not valid' . $this->intWindDirectionInDegrees);
        } else {
            $index = intdiv(intval($this->intWindDirectionInDegrees + 22.5) % 360, 45);
            $this->strWindDirection = self::WIND_DIRECTIONS[$index];
        }
    }

    /**
     * Static function to write everything to the table scgb_resort_forecast.
     *
     * @param array $weatherForecastData - array of WeatherForecast objects
     * @param int $intSCGBResortID - SCGB Resort ID
     * @param string $strResortName - SCGB Resort Name
     * @param string $strResortCountry - SCGB Resort Country
     * @param mysqli $conn - mysqli connection
     *
     * @return void
     * @throws Exception
     */
    public static function writeDataToDB(
        array $weatherForecastData,
        int $intSCGBResortID,
        string $strResortName,
        string $strResortCountry,
        mysqli $conn) : void
    {
        logger()->debug(
            "Writing forecast to database",
            array(
                'resortID' => $intSCGBResortID,
                'resortName' => $strResortName,
                'resortCountry' => $strResortCountry,
                'file' => basename(__FILE__),
                'function' => __FUNCTION__,
                'line' => __LINE__,
            )
        );

        $strSQL = "DELETE FROM scgb_resort_forecast where intSCGBResortID = " . $intSCGBResortID . ";";
        $stmt = $conn->prepare($strSQL);
        $stmt->execute();

        $strSQL = "INSERT INTO scgb_resort_forecast (" .
            "intSCGBResortID, " .
            "dtmLastUpdate, " .
            "strResortName, " .
            "strResortCountry, " .
            "dtmForecastDate, " .
            "strForecastPeriod, " .
            "blnIsTopForecast, " .
            "strWeather, " .
            "intWindSpeedInMPH, " .
            "strWindDirection, " .
            "fltSnowExpectedinCentimeters, " .
            "fltMinTemperature, " .
            "fltMaxTemperature, " .
            "fltFeelsLikeTemperature, " .
            "strWindURL, " .
            "strWeatherURL) " .
            "VALUES (" .
            "%d, now(), '%s', '%s', '%s', '%s', %d, '%s', %d, '%s', %f, %f, %f, %f, '%s', '%s');";

        foreach ($weatherForecastData as $forecastData) {
            // We need to escape possible single quote in the resort name
            $strSQLStatement = sprintf(
                $strSQL,
                $intSCGBResortID,
                str_replace("'", "\'", $strResortName),
                $strResortCountry,
                $forecastData->dtmForecastDate->format('Y-m-d H:i:s'),
                $forecastData->strForecastPeriod,
                $forecastData->blnIsTopForecast,
                $forecastData->strWeather,
                $forecastData->intWindSpeedInMPH,
                $forecastData->strWindDirection,
                $forecastData->fltSnowExpectedinCentimeters,
                $forecastData->fltMinAirTemperature,
                $forecastData->fltMaxAirTemperature,
                $forecastData->fltFeelsLikeTemperature,
                $forecastData->strWindURL,
                $forecastData->strWeatherURL
            );
            logger()->debug("writeResortDataToDB: SQL Statement: " . $strSQLStatement,
                array('file' => basename(__FILE__), 'function' => __FUNCTION__, 'line' => __LINE__,));

            $stmt = $conn->prepare($strSQLStatement);
            $stmt->execute();
        }
    }

    /**
     * Return a string with a comma seperated list of the name of every property in the class.
     *
     * @return string
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function getCSVHeader() : string
    {
        return (implode(",", array_keys(get_object_vars($this))) . "\n");
    }

    /**
     * Return a comma separated string of the values of every property in the class.
     *
     * @return string
     * @throws Exception
     * @noinspection PhpUnused
     */
    public function getCSVRow() : string
    {
        $retString = '';
        foreach (get_object_vars($this) as $value) {
            if (is_null($value)) {
                $retString .= 'null,';
            } elseif (is_a($value, 'DateTime')) {
                $retString .= $value->format('Y-m-d H:i:s') . ',';
            } elseif (is_bool($value)) {
                $retString .= ($value ? 'true' : 'false') . ',';
            } else {
                $retString .= $value . ',';
            }
        }
        // Remove the trailing, and add a carriage return
        return substr($retString, 0, -1) . "\n";
    }

    /**
     * Taking the wind direction and speed, create and image if necessary and then provide q URL.
     *
     * @return void
     * @throws Exception
     */
    private function setWindURL() : void
    {
        // Directory where we keep the raw material
        $dirStockImages = getConfigItem('dirStockImages');

        // Directory under the web root where we keep the images
        $dirWebRoot = getConfigItem('dirWebRoot');
        $dirWebImages = getConfigItem('dirWebImages');

        $stockFilePath = null;
        // A special case is when the wind speed is 0 - in which case we use the calm image
        if ($this->intWindSpeedInMPH == 0 || $this->strWindDirection == '') {
            $stockFilePath = $dirStockImages . '/wind-calm.png';
            $this->strWindURL = "/" . $dirWebImages . '/wind-calm.png';
        } else {
            $this->strWindURL = "/" . $dirWebImages . '/wind_' . $this->strWindDirection . '-' .
                $this->intWindSpeedInMPH . '.png';
        }
        $targetFilePath = $dirWebRoot . $this->strWindURL;

        // If the target file doesn't exist, then we need to create it
        if (!file_exists($targetFilePath)) {
            // if a source file is specified, then copy that to the target
            if (!is_null($stockFilePath)) {
                copy($stockFilePath, $targetFilePath);
            } else {
                // Otherwise, create the image dynamically
                $this->createWindImage($dirStockImages, $targetFilePath);
            }
        }
    }

    /**
     * Create a wind image dynamically
     *
     * TODO Improve the error checking around the image directories
     *
     * @param string $dirStockImages - Directory where we keep the raw material (distributed with the code)
     * @param string $targetFilePath - Full path to the destination file
     * @return void
     * @throws Exception
     */
    private function createWindImage(string $dirStockImages, string $targetFilePath) : void {
        $baseImagefile = $dirStockImages . '/' . self::WIND_BASE_IMAGE;

        // The rotation is specified Anti-clockwise.
        // Also, we have to reverse the direction of the arrow to match the wind direction
        $rotang = match ($this->strWindDirection) {
            'N' => 0,
            'NE' => 325,
            'E' => 270,
            'SE' => 225,
            'S' => 180,
            'SW' => 135,
            'W' => 90,
            'NW' => 45,
            default => throw new Exception('Unknown wind direction: ' . $this->strWindDirection),
        };

        if (!($source = imagecreatefrompng($baseImagefile))) {
            throw new Exception('Failed to open Wind image  file: ' . $baseImagefile);
        }

        $rotation = imagerotate($source, $rotang, imageColorAllocateAlpha($source, 0, 0, 0, 127));

        // If the image rotated by 45/135/225/315 degrees, then its size will have increased (180×180) vs (128×128)
        // So crop it back to 128×128
        if (strlen($this->strWindDirection) == 2) {
            $rotation = imagecrop($rotation, ['x' => 26, 'y' => 26, 'width' => 128, 'height' => 128]);
            $rotation = imagescale($rotation, 128, 128);
        }
        $ypos = 74;
        if ($this->intWindSpeedInMPH < 10) {
            $xpos = 57;
        } else {
            $xpos = 48;
        }

        // Make the image transparent
        imagealphablending($rotation, false);
        imagesavealpha($rotation, true);

        /*
         * Add the wind speed onto the image. The position is determined by the length of the string
         * Winds speeds greater than 99mph (ca. 159 km/h) not supported
         */

        // Set the text colour to white
        $white = imagecolorallocate($rotation, 255, 255, 255);

        // Set Path to Font File TODO - Make this configurable??
        $font_path = __DIR__ . '/Lato-Regular.ttf';
        imagettftext($rotation, 22, 0, $xpos, $ypos, $white, $font_path, strval($this->intWindSpeedInMPH));

        // Save the image to the target file
        imagepng($rotation, $targetFilePath);

        // Clean Up
        imagedestroy($rotation);
        imagedestroy($source);
        imagedestroy($rotation);
    }

    /**
     * Determine if the time specified straddles one of the const's AM, PM or Night.
     *
     * @param DateTime $dtmForecastStart
     * @param DateTime $dtmForecastEnd
     * @return void
     */
    public function setForecastPeriod(
        DateTime $dtmForecastStart,
        DateTime $dtmForecastEnd): void
    {
        $intForecastPeriod = null;

        // Extract the hour from $validFromDate and $validUntilDate
        $validFromHour = intval($dtmForecastStart->format('H'));
        $validUntilHour = intval($dtmForecastEnd->format('H'));
        if ($validUntilHour == 0) {
            $validUntilHour = 24;
        }

        // Cope with the basic case first where the period doesn't cross midnight
        if ($validFromHour < $validUntilHour) {
            if (($validFromHour <= self::AM) && ($validUntilHour > self::AM)) {
                $intForecastPeriod = self::AM;
            } elseif (($validFromHour <= self::PM) && ($validUntilHour > self::PM)) {
                $intForecastPeriod = self::PM;
            } elseif (($validFromHour <= self::NIGHT) && ($validUntilHour > self::NIGHT)) {
                $intForecastPeriod = self::NIGHT;
            }
        } else {
            // Now cope with the case where the period crosses midnight - which will only be for the night period
            // NIGHT = 9, validFrom = 23, validUntil = 2
            if (($validFromHour <= self::NIGHT) || ($validUntilHour > self::NIGHT)) {
                $intForecastPeriod = self::NIGHT;
            }
        }
        $this->strForecastPeriod = null;
        if ($intForecastPeriod !== null) {
            $this->strForecastPeriod = self::ARR_PERIOD_STRINGS[$intForecastPeriod];
        }
    }
}
