use weather_dev;

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';



-- -----------------------------------------------------
-- Table `scgb_resort_forecast`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scgb_resort_forecast` ;

CREATE TABLE IF NOT EXISTS `scgb_resort_forecast` (
  `intSCGBResortID` INT NOT NULL,
  `dtmLastUpdate` DATETIME NOT NULL,
  `strResortName` VARCHAR(45) NOT NULL,
  `strResortCountry` VARCHAR(45) NOT NULL,
  `dtmForecastDate` DATETIME NOT NULL,
  `strForecastPeriod` VARCHAR(8) NOT NULL,
  `blnIsTopForecast` TINYINT NOT NULL,
  `strWeather` VARCHAR(48) NOT NULL,
  `strWeatherURL` VARCHAR(128) NOT NULL,
  `intWindSpeedInMPH` INT NOT NULL,
  `strWindDirection` VARCHAR(2) NOT NULL,
  `strWindURL` VARCHAR(128) NOT NULL,
  `fltSnowExpectedinCentimeters` FLOAT NOT NULL,
  `fltMinTemperature` FLOAT NOT NULL,
  `fltMaxTemperature` FLOAT NOT NULL,
  `fltFeelsLikeTemperature` FLOAT NOT NULL,
  INDEX `index1` (`intSCGBResortID` ASC) VISIBLE,
  INDEX `index2` (`intSCGBResortID` ASC, `dtmForecastDate` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `scgb_resort_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `scgb_resort_data` ;

CREATE TABLE IF NOT EXISTS `scgb_resort_data` (
  `intSCGBResortID` INT NOT NULL,
  `dtmLastUpdate` DATETIME NOT NULL,
  `strResortName` VARCHAR(45) NOT NULL,
  `strResortCountry` VARCHAR(45) NOT NULL,
  `fltMinTemperature` FLOAT NULL,
  `fltMaxTemperature` FLOAT NULL,
  `strWeather` VARCHAR(48) NULL,
  `strWeatherURL` VARCHAR(128) NOT NULL,
  `intSnowLowerPisteInCentimeters` INT NULL,
  `intSnowUpperPisteInCentimeters` FLOAT NULL,
  `fltAmountOfLastSnowInCentimeters` FLOAT NULL,
  `dtmLastSnowed` DATETIME NULL,
  `fltSnowForecastNext3DaysInCentimeters` FLOAT NULL,
  `fltSnowForecastNext6DaysInCentimeters` FLOAT NULL,
  `intLowerWeatherStationElevation` INT NULL,
  `intUpperWeatherStationElevation` INT NULL,
  `strOffPisteSnowConditions` VARCHAR(48) NULL,
  `intLiftsOpen` INT NULL,
  `intLiftsTotal` INT NULL,
  `dtmResortOpening` DATETIME NULL,
  `dtmResortClosing` DATETIME NULL,
  UNIQUE INDEX `intSCGBResortID_UNIQUE` (`intSCGBResortID` ASC) VISIBLE,
  PRIMARY KEY (`intSCGBResortID`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
