<?php
declare(strict_types=1);

namespace SCGB;

use Exception;

// Paranoid check to ensure this file is not accessed via the website
if (key_exists('REQUEST_METHOD', $_SERVER)) {
    if ($_SERVER['REQUEST_METHOD'] == 'GET' && realpath(__FILE__) == realpath($_SERVER['SCRIPT_FILENAME'])) {
        header('HTTP/1.0 403 Forbidden', true, 403);
        /** @noinspection PhpVoidFunctionResultUsedInspection */
        die(header('location: /index.php'));
    }
}

/**
 * Weather & Resort Data Loader for the SkiClub of Great Britain website - https://skiclub.co.uk.
 *
 * Runs periodically (by cron) on the website hosts and loads the following data into the WordPress
 * database for use by the website:
 * - Fundamental Resort Data - this is loaded from a SCGB provided CSV
 * - Details of dynamic resort conditions (if available): lifts open, last snow etc.
 * - 10-Day Weather forecast
 *
 * See gitlab wiki for more details - https://gitlab.com/scgb1/scgb-website-loader/-/wikis/Website-Data-Loader-Wiki
 *
 * Resort Conditions and weather may not be added if the data is not available
 *
 * Configuration options:
 * - the raw data from the external APIs can be written to external files.
 * - the data loaded into the database is written to CSVs to avoid having to access the database externally
 *
 * Two tables in the website database:
 * - scgb_resort_data - data specific to each resort - including current conditions
 * - scgb_resort_forecast - 10-day forecast for each resort broken into Low/High Mountain and then AM, PM, Night
 *
 * Database tables updated in one transaction - either everything works or nothing
 *
 * Error Handling is basic. Any error (including PHP warnings) result in a shutdown.
 *
 * Cleanup of data files and logfiles is performed by a supporting shell script
 *
 */

require_once(__DIR__ . "/" . 'vendor/autoload.php');

require_once(__DIR__ . "/" . 'SCGB/Utils.php');
require_once(__DIR__ . "/" . 'SCGB/ResortData.php');
require_once(__DIR__ . "/" . 'SCGB/SkiService.php');

require_once(__DIR__ . "/" . 'SCGB/Collector_DTN.php');
require_once(__DIR__ . "/" . 'SCGB/Collector_OpenWeather.php');

require_once(__DIR__ . "/" . 'SCGB/Forecast_Base.php');


// Basic initialisation, including setting up logging. The code can't exit cleanly until this is completed
$bulkHandler = initialise();

// Start of the main processing
logger()->info('Started SCGB Weather Loader', array('file' => basename(__FILE__), 'line' => __LINE__));

# Initialise the database connection
try {
    $conn = getDBConnection();
} catch (Exception $e) {
    logger()->emergency(
        'Failed to connect to the Database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

/**
 * Main processing
 */
try {
    // Read the (per resort) data from the Ski Club data file and (if necessary) enrich it with elevation data
    // from the Google Maps Elevation API
    $arrResortData = ResortData::getResortData();

    // Reconcile the Ski Club data with the website
    ResortData::reconcileResortDataWithWebsite($conn);

    // Combine the Ski Club data with the Ski Service data downloaded from their API.
    // Note that they don't supply data for all resorts
    $skiServiceCollector = new SkiService();
    ResortData::addSkiServiceData($arrResortData, $skiServiceCollector);

    // Add the weather forecast and set the current conditions in the resort data
    // See which weather forecast API to use
    if (getConfigItem('openWeatherAPIKey', false)) {
        $collector = new Collector_OpenWeather();
    } else {
        $collector = new Collector_DTN();
    }
    ResortData::addWeatherForecasts($arrResortData, $collector);

    // Save the resort and forecast data to timestamped CSV files
    if ($strResortDataFile = getDataFileName('saveResortDataToDir', false)) {
        ResortData::saveToCSV($arrResortData, $strResortDataFile);
    }
} catch (Exception $e) {
    logger()->emergency(
        'Failed whilst gathering data: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

// Save everything to the database using a single transaction
try {
    $conn->begin_transaction();
    ResortData::writeDataToDB($arrResortData, $conn);
} catch (Exception $e) {
    logger()->emergency(
        'Failed to write to the database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    $conn->rollback();
    $conn->close();
    die(1);
}

// Commit and close the database connection
try {
    $conn->commit();
    $conn->close();
} catch (Exception $e) {
    logger()->emergency(
        'Failed to commit data to the database: ' . $e->getMessage(),
        array('file' => basename(__FILE__), 'line' => __LINE__)
    );
    die(1);
}

if ($bulkHandler !== null) {
    // Flush the email log handler, this means only send one email is sent per run
    $bulkHandler->flush();
    $bulkHandler->close();
}

// Summary of data loaded
logger()->info('Loaded ' . count($arrResortData) . ' resorts and ' .
    ResortData::getForecastCount($arrResortData) . ' forecasts',
    array('file' => basename(__FILE__), 'line' => __LINE__));

logger()->info(
    'Finished SCGB Resort & Weather Loader',
    array('file' => basename(__FILE__), 'line' => __LINE__)
);
exit(0);
